// uScript Action Node
// (C) 2012 hyenApp LLC

using UnityEngine;
using System.Collections;

[NodePath("Conditions/Switches")]

[NodeCopyright("Copyright 2012 by hyenApp LLC")]
[NodeToolTip("Manually pick a Platform output to fire the signal to.")]
[NodeAuthor("hyenApp LLC", "http://www.hyenapp.com")]
[NodeHelp("")]

[FriendlyName("Platform Switch", "Manually pick a Platform output to fire the signal to.\n\nIf no Output is specified the Application.platform value will be used.")]
public class hyenApp_PlatformSwitch : uScriptLogic {

	private RuntimePlatform m_CurrentOutput = Application.platform;
	private bool m_SwitchOpen = true;

	public delegate void uScriptEventHandler(object sender, System.EventArgs args);
	[FriendlyName("OSXEditor", "In the Unity editor on Mac OS X")] public event uScriptEventHandler OSXEditor;
	[FriendlyName("WindowsEditor", "In the Unity editor on Windows")] public event uScriptEventHandler WindowsEditor;
	[FriendlyName("OSXPlayer", "In the player on Mac OS X")] public event uScriptEventHandler OSXPlayer;
	[FriendlyName("WindowsPlayer", "In the player on Windows")] public event uScriptEventHandler WindowsPlayer;
	[FriendlyName("OSXDashboardPlayer", "In the Dashboard widget on Mac OS X")] public event uScriptEventHandler OSXDashboardPlayer;
	[FriendlyName("OSXWebPlayer", "In the web player on Mac OS X")] public event uScriptEventHandler OSXWebPlayer;
	[FriendlyName("WindowsWebPlayer", "In the web player on Windows")] public event uScriptEventHandler WindowsWebPlayer;
	[FriendlyName("IPhonePlayer", "In the player on the iPhone")] public event uScriptEventHandler IPhonePlayer;
	[FriendlyName("Android", "In the player on Android devices")] public event uScriptEventHandler Android;
	[FriendlyName("WiiPlayer", "In the player on Nintendo Wii")] public event uScriptEventHandler WiiPlayer;
	[FriendlyName("XBOX360", "In the player on the XBOX360")] public event uScriptEventHandler XBOX360;
	[FriendlyName("PS3", "In the player on the Play Station 3")] public event uScriptEventHandler PS3;
	[FriendlyName("NaCl", "Google Native Client")] public event uScriptEventHandler NaCl;
	[FriendlyName("FlashPlayer", "Flash Player")] public event uScriptEventHandler FlashPlayer;
	[FriendlyName("Unknown", "")] public event uScriptEventHandler Unknown;

	public void In(
		[FriendlyName("Output To Use", "The output switch to use.")] RuntimePlatform CurrentOutput
	) {
		m_CurrentOutput = CurrentOutput;
		
		// Set correct output socket to true
		if (m_SwitchOpen) {
			switch (m_CurrentOutput) {
				case RuntimePlatform.OSXEditor:
					OSXEditor(this, new System.EventArgs());
					break;

				case RuntimePlatform.OSXPlayer:
					OSXPlayer(this, new System.EventArgs());
					break;

				case RuntimePlatform.WindowsPlayer:
					WindowsPlayer(this, new System.EventArgs());
					break;

				case RuntimePlatform.OSXWebPlayer:
					OSXWebPlayer(this, new System.EventArgs());
					break;

				case RuntimePlatform.OSXDashboardPlayer:
					OSXDashboardPlayer(this, new System.EventArgs());
					break;

				case RuntimePlatform.WindowsWebPlayer:
					WindowsWebPlayer(this, new System.EventArgs());
					break;
				
				//case RuntimePlatform.WiiPlayer:
				//	WiiPlayer(this, new System.EventArgs());
				//	break;
				//
				case RuntimePlatform.WindowsEditor:
					WindowsEditor(this, new System.EventArgs());
					break;
				
				case RuntimePlatform.IPhonePlayer:
					IPhonePlayer(this, new System.EventArgs());
					break;
				
				case RuntimePlatform.XBOX360:
					XBOX360(this, new System.EventArgs());
					break;
				
				case RuntimePlatform.PS3:
					PS3(this, new System.EventArgs());
					break;
				
				case RuntimePlatform.Android:
					Android(this, new System.EventArgs());
					break;
				
				case RuntimePlatform.NaCl:
					NaCl(this, new System.EventArgs());
					break;
				
				case RuntimePlatform.FlashPlayer:
					FlashPlayer(this, new System.EventArgs());
					break;

				default:
					Unknown(this, new System.EventArgs());
					break;
			}
			
		}
		
	}

}
