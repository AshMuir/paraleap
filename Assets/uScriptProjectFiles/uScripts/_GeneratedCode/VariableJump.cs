//uScript Generated Code - Build 1.0.3072
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class VariableJump : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public System.Boolean IsGrounded = (bool) false;
   System.Single local_10_System_Single = (float) 0;
   System.Single local_12_System_Single = (float) 0;
   System.Single local_13_System_Single = (float) 0;
   System.Single local_15_System_Single = (float) -1;
   System.Single local_23_System_Single = (float) 2;
   System.Single local_25_System_Single = (float) 0;
   System.Single local_26_System_Single = (float) 0;
   UnityEngine.Vector2 local_33_UnityEngine_Vector2 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector2 local_36_UnityEngine_Vector2 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector2 local_40_UnityEngine_Vector2 = new Vector2( (float)0, (float)0 );
   System.Single local_41_System_Single = (float) 0;
   UnityEngine.Vector2 local_46_UnityEngine_Vector2 = new Vector2( (float)0, (float)0 );
   System.Single local_48_System_Single = (float) 0;
   System.Single local_50_System_Single = (float) 0;
   UnityEngine.Vector2 local_52_UnityEngine_Vector2 = new Vector2( (float)0, (float)0 );
   UnityEngine.Vector2 local_64_UnityEngine_Vector2 = new Vector2( (float)0, (float)-1 );
   UnityEngine.Vector3 local_66_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector2 local_68_UnityEngine_Vector2 = new Vector2( (float)0, (float)0 );
   System.Single local_72_System_Single = (float) 0;
   System.Single local_73_System_Single = (float) 0;
   System.Single local_75_System_Single = (float) 5;
   System.Single local_77_System_Single = (float) 0.01;
   System.Single local_8_System_Single = (float) 2;
   UnityEngine.Vector3 local_81_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_83_System_Single = (float) 0;
   System.Single local_85_System_Single = (float) 0;
   UnityEngine.Vector2 local_91_UnityEngine_Vector2 = new Vector2( (float)0, (float)0 );
   System.Single local_93_System_Single = (float) 0;
   System.Single local_AbsGravity_System_Single = (float) 0;
   System.Single local_Gravity_System_Single = (float) 0;
   System.Single local_Grounding_distance_System_Single = (float) 0;
   UnityEngine.Vector2 local_hit_location_V2_UnityEngine_Vector2 = new Vector2( (float)0, (float)0 );
   UnityEngine.GameObject local_Hit_Object_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_Hit_Object_UnityEngine_GameObject_previous = null;
   System.Single local_MaxJumpHeightVelocity_System_Single = (float) 0;
   System.Single local_MinJumpHeightVelocity_System_Single = (float) 0;
   UnityEngine.Transform local_raycastpoint_transform_UnityEngine_Transform = default(UnityEngine.Transform);
   public System.Single MaxJumpHeight = (float) 4;
   public System.Single MinJumpHeight = (float) 1;
   public UnityEngine.GameObject RaycastPoint = default(UnityEngine.GameObject);
   UnityEngine.GameObject RaycastPoint_previous = null;
   public System.Single TimeToApex = (float) 1;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_30 = null;
   UnityEngine.GameObject owner_Connection_44 = null;
   UnityEngine.GameObject owner_Connection_56 = null;
   UnityEngine.GameObject owner_Connection_57 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_1 = UnityEngine.KeyCode.Space;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_1 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_1 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_1 = true;
   //pointer to script instanced logic node
   uScript_GraphEvents logic_uScript_GraphEvents_uScript_GraphEvents_5 = new uScript_GraphEvents( );
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_7 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_7 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_7 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_7;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_7;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_7 = true;
   //pointer to script instanced logic node
   uScriptAct_DivideFloat logic_uScriptAct_DivideFloat_uScriptAct_DivideFloat_9 = new uScriptAct_DivideFloat( );
   System.Single logic_uScriptAct_DivideFloat_A_9 = (float) 0;
   System.Single logic_uScriptAct_DivideFloat_B_9 = (float) 0;
   System.Single logic_uScriptAct_DivideFloat_FloatResult_9;
   System.Int32 logic_uScriptAct_DivideFloat_IntResult_9;
   bool logic_uScriptAct_DivideFloat_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_11 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_11 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_11 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_11;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_11;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_11 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_14 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_14 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_14 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_14;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_14;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_14 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_19 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_19 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_19 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_19;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_19;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_19 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_22 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_22 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_22 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_22;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_22;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_22 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_24 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_24 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_24 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_24;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_24;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_24 = true;
   //pointer to script instanced logic node
   uScriptAct_ReplaceComponentsVector2 logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_31 = new uScriptAct_ReplaceComponentsVector2( );
   UnityEngine.Vector2 logic_uScriptAct_ReplaceComponentsVector2_Target_31 = new Vector2( );
   System.Single logic_uScriptAct_ReplaceComponentsVector2_X_31 = (float) 0;
   System.Boolean logic_uScriptAct_ReplaceComponentsVector2_IgnoreX_31 = (bool) false;
   System.Single logic_uScriptAct_ReplaceComponentsVector2_Y_31 = (float) 0;
   System.Boolean logic_uScriptAct_ReplaceComponentsVector2_IgnoreY_31 = (bool) false;
   UnityEngine.Vector2 logic_uScriptAct_ReplaceComponentsVector2_Output_31;
   bool logic_uScriptAct_ReplaceComponentsVector2_Out_31 = true;
   //pointer to script instanced logic node
   uScriptAct_GetRigidbodyVelocity2D logic_uScriptAct_GetRigidbodyVelocity2D_uScriptAct_GetRigidbodyVelocity2D_32 = new uScriptAct_GetRigidbodyVelocity2D( );
   UnityEngine.GameObject logic_uScriptAct_GetRigidbodyVelocity2D_Target_32 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 logic_uScriptAct_GetRigidbodyVelocity2D_Velocity_32;
   System.Single logic_uScriptAct_GetRigidbodyVelocity2D_AngularVelocity_32;
   System.Single logic_uScriptAct_GetRigidbodyVelocity2D_Magnitude_32;
   bool logic_uScriptAct_GetRigidbodyVelocity2D_Out_32 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareFloat logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_34 = new uScriptCon_CompareFloat( );
   System.Single logic_uScriptCon_CompareFloat_A_34 = (float) 0;
   System.Single logic_uScriptCon_CompareFloat_B_34 = (float) 0;
   bool logic_uScriptCon_CompareFloat_GreaterThan_34 = true;
   bool logic_uScriptCon_CompareFloat_GreaterThanOrEqualTo_34 = true;
   bool logic_uScriptCon_CompareFloat_EqualTo_34 = true;
   bool logic_uScriptCon_CompareFloat_NotEqualTo_34 = true;
   bool logic_uScriptCon_CompareFloat_LessThanOrEqualTo_34 = true;
   bool logic_uScriptCon_CompareFloat_LessThan_34 = true;
   //pointer to script instanced logic node
   uScriptAct_ReplaceComponentsVector2 logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_35 = new uScriptAct_ReplaceComponentsVector2( );
   UnityEngine.Vector2 logic_uScriptAct_ReplaceComponentsVector2_Target_35 = new Vector2( );
   System.Single logic_uScriptAct_ReplaceComponentsVector2_X_35 = (float) 0;
   System.Boolean logic_uScriptAct_ReplaceComponentsVector2_IgnoreX_35 = (bool) false;
   System.Single logic_uScriptAct_ReplaceComponentsVector2_Y_35 = (float) 0;
   System.Boolean logic_uScriptAct_ReplaceComponentsVector2_IgnoreY_35 = (bool) false;
   UnityEngine.Vector2 logic_uScriptAct_ReplaceComponentsVector2_Output_35;
   bool logic_uScriptAct_ReplaceComponentsVector2_Out_35 = true;
   //pointer to script instanced logic node
   uScriptAct_GetComponentsVector2 logic_uScriptAct_GetComponentsVector2_uScriptAct_GetComponentsVector2_38 = new uScriptAct_GetComponentsVector2( );
   UnityEngine.Vector2 logic_uScriptAct_GetComponentsVector2_InputVector2_38 = new Vector2( );
   System.Single logic_uScriptAct_GetComponentsVector2_X_38;
   System.Single logic_uScriptAct_GetComponentsVector2_Y_38;
   bool logic_uScriptAct_GetComponentsVector2_Out_38 = true;
   //pointer to script instanced logic node
   uScriptAct_GetRigidbodyVelocity2D logic_uScriptAct_GetRigidbodyVelocity2D_uScriptAct_GetRigidbodyVelocity2D_39 = new uScriptAct_GetRigidbodyVelocity2D( );
   UnityEngine.GameObject logic_uScriptAct_GetRigidbodyVelocity2D_Target_39 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 logic_uScriptAct_GetRigidbodyVelocity2D_Velocity_39;
   System.Single logic_uScriptAct_GetRigidbodyVelocity2D_AngularVelocity_39;
   System.Single logic_uScriptAct_GetRigidbodyVelocity2D_Magnitude_39;
   bool logic_uScriptAct_GetRigidbodyVelocity2D_Out_39 = true;
   //pointer to script instanced logic node
   uScriptAct_SetRigidbodyVelocity2D logic_uScriptAct_SetRigidbodyVelocity2D_uScriptAct_SetRigidbodyVelocity2D_42 = new uScriptAct_SetRigidbodyVelocity2D( );
   UnityEngine.GameObject[] logic_uScriptAct_SetRigidbodyVelocity2D_Target_42 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector2 logic_uScriptAct_SetRigidbodyVelocity2D_Velocity_42 = new Vector2( );
   bool logic_uScriptAct_SetRigidbodyVelocity2D_Out_42 = true;
   //pointer to script instanced logic node
   uScriptAct_GetComponentsVector2 logic_uScriptAct_GetComponentsVector2_uScriptAct_GetComponentsVector2_45 = new uScriptAct_GetComponentsVector2( );
   UnityEngine.Vector2 logic_uScriptAct_GetComponentsVector2_InputVector2_45 = new Vector2( );
   System.Single logic_uScriptAct_GetComponentsVector2_X_45;
   System.Single logic_uScriptAct_GetComponentsVector2_Y_45;
   bool logic_uScriptAct_GetComponentsVector2_Out_45 = true;
   //pointer to script instanced logic node
   uScriptAct_GetRigidbodyVelocity2D logic_uScriptAct_GetRigidbodyVelocity2D_uScriptAct_GetRigidbodyVelocity2D_47 = new uScriptAct_GetRigidbodyVelocity2D( );
   UnityEngine.GameObject logic_uScriptAct_GetRigidbodyVelocity2D_Target_47 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 logic_uScriptAct_GetRigidbodyVelocity2D_Velocity_47;
   System.Single logic_uScriptAct_GetRigidbodyVelocity2D_AngularVelocity_47;
   System.Single logic_uScriptAct_GetRigidbodyVelocity2D_Magnitude_47;
   bool logic_uScriptAct_GetRigidbodyVelocity2D_Out_47 = true;
   //pointer to script instanced logic node
   uScriptAct_AddFloat_v2 logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_49 = new uScriptAct_AddFloat_v2( );
   System.Single logic_uScriptAct_AddFloat_v2_A_49 = (float) 0;
   System.Single logic_uScriptAct_AddFloat_v2_B_49 = (float) 0;
   System.Single logic_uScriptAct_AddFloat_v2_FloatResult_49;
   System.Int32 logic_uScriptAct_AddFloat_v2_IntResult_49;
   bool logic_uScriptAct_AddFloat_v2_Out_49 = true;
   //pointer to script instanced logic node
   uScriptAct_ReplaceComponentsVector2 logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_51 = new uScriptAct_ReplaceComponentsVector2( );
   UnityEngine.Vector2 logic_uScriptAct_ReplaceComponentsVector2_Target_51 = new Vector2( );
   System.Single logic_uScriptAct_ReplaceComponentsVector2_X_51 = (float) 0;
   System.Boolean logic_uScriptAct_ReplaceComponentsVector2_IgnoreX_51 = (bool) false;
   System.Single logic_uScriptAct_ReplaceComponentsVector2_Y_51 = (float) 0;
   System.Boolean logic_uScriptAct_ReplaceComponentsVector2_IgnoreY_51 = (bool) false;
   UnityEngine.Vector2 logic_uScriptAct_ReplaceComponentsVector2_Output_51;
   bool logic_uScriptAct_ReplaceComponentsVector2_Out_51 = true;
   //pointer to script instanced logic node
   uScriptAct_SetRigidbodyVelocity2D logic_uScriptAct_SetRigidbodyVelocity2D_uScriptAct_SetRigidbodyVelocity2D_53 = new uScriptAct_SetRigidbodyVelocity2D( );
   UnityEngine.GameObject[] logic_uScriptAct_SetRigidbodyVelocity2D_Target_53 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector2 logic_uScriptAct_SetRigidbodyVelocity2D_Velocity_53 = new Vector2( );
   bool logic_uScriptAct_SetRigidbodyVelocity2D_Out_53 = true;
   //pointer to script instanced logic node
   uScriptAct_SetRigidbodyVelocity2D logic_uScriptAct_SetRigidbodyVelocity2D_uScriptAct_SetRigidbodyVelocity2D_55 = new uScriptAct_SetRigidbodyVelocity2D( );
   UnityEngine.GameObject[] logic_uScriptAct_SetRigidbodyVelocity2D_Target_55 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector2 logic_uScriptAct_SetRigidbodyVelocity2D_Velocity_55 = new Vector2( );
   bool logic_uScriptAct_SetRigidbodyVelocity2D_Out_55 = true;
   //pointer to script instanced logic node
   uScriptAct_Raycast2D logic_uScriptAct_Raycast2D_uScriptAct_Raycast2D_59 = new uScriptAct_Raycast2D( );
   System.Object logic_uScriptAct_Raycast2D_Start_59 = "";
   UnityEngine.Vector2 logic_uScriptAct_Raycast2D_Direction_59 = new Vector2( );
   System.Single logic_uScriptAct_Raycast2D_Distance_59 = (float) 0;
   UnityEngine.LayerMask logic_uScriptAct_Raycast2D_layerMask_59 = 1;
   System.Boolean logic_uScriptAct_Raycast2D_include_59 = (bool) true;
   System.Boolean logic_uScriptAct_Raycast2D_showRay_59 = (bool) true;
   UnityEngine.GameObject logic_uScriptAct_Raycast2D_HitObject_59;
   UnityEngine.Vector3 logic_uScriptAct_Raycast2D_HitLocation_59;
   UnityEngine.Vector3 logic_uScriptAct_Raycast2D_HitNormal_59;
   bool logic_uScriptAct_Raycast2D_NotObstructed_59 = true;
   bool logic_uScriptAct_Raycast2D_Obstructed_59 = true;
   //pointer to script instanced logic node
   uScriptAct_GetTransform logic_uScriptAct_GetTransform_uScriptAct_GetTransform_61 = new uScriptAct_GetTransform( );
   UnityEngine.GameObject logic_uScriptAct_GetTransform_Target_61 = default(UnityEngine.GameObject);
   UnityEngine.Transform logic_uScriptAct_GetTransform_targetTransform_61;
   bool logic_uScriptAct_GetTransform_Out_61 = true;
   //pointer to script instanced logic node
   uScript_GraphEvents logic_uScript_GraphEvents_uScript_GraphEvents_62 = new uScript_GraphEvents( );
   //pointer to script instanced logic node
   uScriptAct_GetVector2Distance logic_uScriptAct_GetVector2Distance_uScriptAct_GetVector2Distance_67 = new uScriptAct_GetVector2Distance( );
   UnityEngine.Vector2 logic_uScriptAct_GetVector2Distance_A_67 = new Vector2( );
   UnityEngine.Vector2 logic_uScriptAct_GetVector2Distance_B_67 = new Vector2( );
   System.Single logic_uScriptAct_GetVector2Distance_Distance_67;
   bool logic_uScriptAct_GetVector2Distance_Out_67 = true;
   //pointer to script instanced logic node
   uScriptAct_ReplaceComponentsVector2 logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_70 = new uScriptAct_ReplaceComponentsVector2( );
   UnityEngine.Vector2 logic_uScriptAct_ReplaceComponentsVector2_Target_70 = new Vector2( );
   System.Single logic_uScriptAct_ReplaceComponentsVector2_X_70 = (float) 0;
   System.Boolean logic_uScriptAct_ReplaceComponentsVector2_IgnoreX_70 = (bool) false;
   System.Single logic_uScriptAct_ReplaceComponentsVector2_Y_70 = (float) 0;
   System.Boolean logic_uScriptAct_ReplaceComponentsVector2_IgnoreY_70 = (bool) false;
   UnityEngine.Vector2 logic_uScriptAct_ReplaceComponentsVector2_Output_70;
   bool logic_uScriptAct_ReplaceComponentsVector2_Out_70 = true;
   //pointer to script instanced logic node
   uScriptAct_GetComponentsVector3 logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_71 = new uScriptAct_GetComponentsVector3( );
   UnityEngine.Vector3 logic_uScriptAct_GetComponentsVector3_InputVector3_71 = new Vector3( );
   System.Single logic_uScriptAct_GetComponentsVector3_X_71;
   System.Single logic_uScriptAct_GetComponentsVector3_Y_71;
   System.Single logic_uScriptAct_GetComponentsVector3_Z_71;
   bool logic_uScriptAct_GetComponentsVector3_Out_71 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareFloat logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_76 = new uScriptCon_CompareFloat( );
   System.Single logic_uScriptCon_CompareFloat_A_76 = (float) 0;
   System.Single logic_uScriptCon_CompareFloat_B_76 = (float) 0;
   bool logic_uScriptCon_CompareFloat_GreaterThan_76 = true;
   bool logic_uScriptCon_CompareFloat_GreaterThanOrEqualTo_76 = true;
   bool logic_uScriptCon_CompareFloat_EqualTo_76 = true;
   bool logic_uScriptCon_CompareFloat_NotEqualTo_76 = true;
   bool logic_uScriptCon_CompareFloat_LessThanOrEqualTo_76 = true;
   bool logic_uScriptCon_CompareFloat_LessThan_76 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_79 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_79;
   bool logic_uScriptAct_SetBool_Out_79 = true;
   bool logic_uScriptAct_SetBool_SetTrue_79 = true;
   bool logic_uScriptAct_SetBool_SetFalse_79 = true;
   //pointer to script instanced logic node
   uScriptAct_GetPositionFromTransform logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_80 = new uScriptAct_GetPositionFromTransform( );
   UnityEngine.Transform logic_uScriptAct_GetPositionFromTransform_target_80 = default(UnityEngine.Transform);
   System.Boolean logic_uScriptAct_GetPositionFromTransform_getLocal_80 = (bool) false;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_position_80;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_forward_80;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_right_80;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_up_80;
   UnityEngine.Matrix4x4 logic_uScriptAct_GetPositionFromTransform_worldMatrix_80;
   bool logic_uScriptAct_GetPositionFromTransform_Out_80 = true;
   //pointer to script instanced logic node
   uScriptAct_ReplaceComponentsVector2 logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_82 = new uScriptAct_ReplaceComponentsVector2( );
   UnityEngine.Vector2 logic_uScriptAct_ReplaceComponentsVector2_Target_82 = new Vector2( );
   System.Single logic_uScriptAct_ReplaceComponentsVector2_X_82 = (float) 0;
   System.Boolean logic_uScriptAct_ReplaceComponentsVector2_IgnoreX_82 = (bool) false;
   System.Single logic_uScriptAct_ReplaceComponentsVector2_Y_82 = (float) 0;
   System.Boolean logic_uScriptAct_ReplaceComponentsVector2_IgnoreY_82 = (bool) false;
   UnityEngine.Vector2 logic_uScriptAct_ReplaceComponentsVector2_Output_82;
   bool logic_uScriptAct_ReplaceComponentsVector2_Out_82 = true;
   //pointer to script instanced logic node
   uScriptAct_GetComponentsVector3 logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_84 = new uScriptAct_GetComponentsVector3( );
   UnityEngine.Vector3 logic_uScriptAct_GetComponentsVector3_InputVector3_84 = new Vector3( );
   System.Single logic_uScriptAct_GetComponentsVector3_X_84;
   System.Single logic_uScriptAct_GetComponentsVector3_Y_84;
   System.Single logic_uScriptAct_GetComponentsVector3_Z_84;
   bool logic_uScriptAct_GetComponentsVector3_Out_84 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_89 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_89 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_89 = true;
   bool logic_uScriptCon_CompareBool_False_89 = true;
   //pointer to script instanced logic node
   uScriptAct_SetRigidbodyVelocity2D logic_uScriptAct_SetRigidbodyVelocity2D_uScriptAct_SetRigidbodyVelocity2D_90 = new uScriptAct_SetRigidbodyVelocity2D( );
   UnityEngine.GameObject[] logic_uScriptAct_SetRigidbodyVelocity2D_Target_90 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector2 logic_uScriptAct_SetRigidbodyVelocity2D_Velocity_90 = new Vector2( );
   bool logic_uScriptAct_SetRigidbodyVelocity2D_Out_90 = true;
   //pointer to script instanced logic node
   uScriptAct_ReplaceComponentsVector2 logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_94 = new uScriptAct_ReplaceComponentsVector2( );
   UnityEngine.Vector2 logic_uScriptAct_ReplaceComponentsVector2_Target_94 = new Vector2( );
   System.Single logic_uScriptAct_ReplaceComponentsVector2_X_94 = (float) 0;
   System.Boolean logic_uScriptAct_ReplaceComponentsVector2_IgnoreX_94 = (bool) false;
   System.Single logic_uScriptAct_ReplaceComponentsVector2_Y_94 = (float) 0;
   System.Boolean logic_uScriptAct_ReplaceComponentsVector2_IgnoreY_94 = (bool) false;
   UnityEngine.Vector2 logic_uScriptAct_ReplaceComponentsVector2_Output_94;
   bool logic_uScriptAct_ReplaceComponentsVector2_Out_94 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_43 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_58 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_86 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_92 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_value_17 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_17 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_f_27 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_27 = (float) 0;
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( RaycastPoint_previous != RaycastPoint || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         RaycastPoint_previous = RaycastPoint;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Hit_Object_UnityEngine_GameObject_previous != local_Hit_Object_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Hit_Object_UnityEngine_GameObject_previous = local_Hit_Object_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_30 || false == m_RegisteredForEvents )
      {
         owner_Connection_30 = parentGameObject;
      }
      if ( null == owner_Connection_44 || false == m_RegisteredForEvents )
      {
         owner_Connection_44 = parentGameObject;
      }
      if ( null == owner_Connection_56 || false == m_RegisteredForEvents )
      {
         owner_Connection_56 = parentGameObject;
      }
      if ( null == owner_Connection_57 || false == m_RegisteredForEvents )
      {
         owner_Connection_57 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( RaycastPoint_previous != RaycastPoint || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         RaycastPoint_previous = RaycastPoint;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Hit_Object_UnityEngine_GameObject_previous != local_Hit_Object_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Hit_Object_UnityEngine_GameObject_previous = local_Hit_Object_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Input component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Input>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Input>();
               }
               if ( null != component )
               {
                  component.KeyEvent += Instance_KeyEvent_0;
               }
            }
         }
      }
      if ( null == event_UnityEngine_GameObject_Instance_43 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_43 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_43 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_43.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_43.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_43;
                  component.OnLateUpdate += Instance_OnLateUpdate_43;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_43;
               }
            }
         }
      }
      if ( null == event_UnityEngine_GameObject_Instance_58 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_58 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_58 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_58.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_58.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_58;
                  component.OnLateUpdate += Instance_OnLateUpdate_58;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_58;
               }
            }
         }
      }
      if ( null == event_UnityEngine_GameObject_Instance_86 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_86 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_86 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_86.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_86.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_86;
                  component.OnLateUpdate += Instance_OnLateUpdate_86;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_86;
               }
            }
         }
      }
      if ( null == event_UnityEngine_GameObject_Instance_92 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_92 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_92 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_92.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_92.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_92;
                  component.OnLateUpdate += Instance_OnLateUpdate_92;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_92;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Input component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Input>();
            if ( null != component )
            {
               component.KeyEvent -= Instance_KeyEvent_0;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_43 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_43.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_43;
               component.OnLateUpdate -= Instance_OnLateUpdate_43;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_43;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_58 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_58.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_58;
               component.OnLateUpdate -= Instance_OnLateUpdate_58;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_58;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_86 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_86.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_86;
               component.OnLateUpdate -= Instance_OnLateUpdate_86;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_86;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_92 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_92.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_92;
               component.OnLateUpdate -= Instance_OnLateUpdate_92;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_92;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1.SetParent(g);
      logic_uScript_GraphEvents_uScript_GraphEvents_5.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_7.SetParent(g);
      logic_uScriptAct_DivideFloat_uScriptAct_DivideFloat_9.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_11.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_14.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_19.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_22.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_24.SetParent(g);
      logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_31.SetParent(g);
      logic_uScriptAct_GetRigidbodyVelocity2D_uScriptAct_GetRigidbodyVelocity2D_32.SetParent(g);
      logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_34.SetParent(g);
      logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_35.SetParent(g);
      logic_uScriptAct_GetComponentsVector2_uScriptAct_GetComponentsVector2_38.SetParent(g);
      logic_uScriptAct_GetRigidbodyVelocity2D_uScriptAct_GetRigidbodyVelocity2D_39.SetParent(g);
      logic_uScriptAct_SetRigidbodyVelocity2D_uScriptAct_SetRigidbodyVelocity2D_42.SetParent(g);
      logic_uScriptAct_GetComponentsVector2_uScriptAct_GetComponentsVector2_45.SetParent(g);
      logic_uScriptAct_GetRigidbodyVelocity2D_uScriptAct_GetRigidbodyVelocity2D_47.SetParent(g);
      logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_49.SetParent(g);
      logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_51.SetParent(g);
      logic_uScriptAct_SetRigidbodyVelocity2D_uScriptAct_SetRigidbodyVelocity2D_53.SetParent(g);
      logic_uScriptAct_SetRigidbodyVelocity2D_uScriptAct_SetRigidbodyVelocity2D_55.SetParent(g);
      logic_uScriptAct_Raycast2D_uScriptAct_Raycast2D_59.SetParent(g);
      logic_uScriptAct_GetTransform_uScriptAct_GetTransform_61.SetParent(g);
      logic_uScript_GraphEvents_uScript_GraphEvents_62.SetParent(g);
      logic_uScriptAct_GetVector2Distance_uScriptAct_GetVector2Distance_67.SetParent(g);
      logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_70.SetParent(g);
      logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_71.SetParent(g);
      logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_76.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_79.SetParent(g);
      logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_80.SetParent(g);
      logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_82.SetParent(g);
      logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_84.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_89.SetParent(g);
      logic_uScriptAct_SetRigidbodyVelocity2D_uScriptAct_SetRigidbodyVelocity2D_90.SetParent(g);
      logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_94.SetParent(g);
      owner_Connection_30 = parentGameObject;
      owner_Connection_44 = parentGameObject;
      owner_Connection_56 = parentGameObject;
      owner_Connection_57 = parentGameObject;
   }
   public void Awake()
   {
      
      logic_uScript_GraphEvents_uScript_GraphEvents_5.uScriptEnable += uScript_GraphEvents_uScriptEnable_5;
      logic_uScript_GraphEvents_uScript_GraphEvents_5.uScriptDisable += uScript_GraphEvents_uScriptDisable_5;
      logic_uScript_GraphEvents_uScript_GraphEvents_5.uScriptDestroy += uScript_GraphEvents_uScriptDestroy_5;
      logic_uScript_GraphEvents_uScript_GraphEvents_62.uScriptEnable += uScript_GraphEvents_uScriptEnable_62;
      logic_uScript_GraphEvents_uScript_GraphEvents_62.uScriptDisable += uScript_GraphEvents_uScriptDisable_62;
      logic_uScript_GraphEvents_uScript_GraphEvents_62.uScriptDestroy += uScript_GraphEvents_uScriptDestroy_62;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
      logic_uScript_GraphEvents_uScript_GraphEvents_5.OnEnable( );
      logic_uScript_GraphEvents_uScript_GraphEvents_62.OnEnable( );
   }
   
   public void OnDisable()
   {
      logic_uScript_GraphEvents_uScript_GraphEvents_5.OnDisable( );
      logic_uScript_GraphEvents_uScript_GraphEvents_62.OnDisable( );
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
      logic_uScript_GraphEvents_uScript_GraphEvents_5.OnDestroy( );
      logic_uScript_GraphEvents_uScript_GraphEvents_62.OnDestroy( );
      logic_uScript_GraphEvents_uScript_GraphEvents_5.uScriptEnable -= uScript_GraphEvents_uScriptEnable_5;
      logic_uScript_GraphEvents_uScript_GraphEvents_5.uScriptDisable -= uScript_GraphEvents_uScriptDisable_5;
      logic_uScript_GraphEvents_uScript_GraphEvents_5.uScriptDestroy -= uScript_GraphEvents_uScriptDestroy_5;
      logic_uScript_GraphEvents_uScript_GraphEvents_62.uScriptEnable -= uScript_GraphEvents_uScriptEnable_62;
      logic_uScript_GraphEvents_uScript_GraphEvents_62.uScriptDisable -= uScript_GraphEvents_uScriptDisable_62;
      logic_uScript_GraphEvents_uScript_GraphEvents_62.uScriptDestroy -= uScript_GraphEvents_uScriptDestroy_62;
   }
   
   void Instance_KeyEvent_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_KeyEvent_0( );
   }
   
   void Instance_OnUpdate_43(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_43( );
   }
   
   void Instance_OnLateUpdate_43(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_43( );
   }
   
   void Instance_OnFixedUpdate_43(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_43( );
   }
   
   void Instance_OnUpdate_58(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_58( );
   }
   
   void Instance_OnLateUpdate_58(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_58( );
   }
   
   void Instance_OnFixedUpdate_58(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_58( );
   }
   
   void Instance_OnUpdate_86(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_86( );
   }
   
   void Instance_OnLateUpdate_86(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_86( );
   }
   
   void Instance_OnFixedUpdate_86(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_86( );
   }
   
   void Instance_OnUpdate_92(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_92( );
   }
   
   void Instance_OnLateUpdate_92(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_92( );
   }
   
   void Instance_OnFixedUpdate_92(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_92( );
   }
   
   void uScript_GraphEvents_uScriptEnable_5(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptEnable_5( );
   }
   
   void uScript_GraphEvents_uScriptDisable_5(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptDisable_5( );
   }
   
   void uScript_GraphEvents_uScriptDestroy_5(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptDestroy_5( );
   }
   
   void uScript_GraphEvents_uScriptEnable_62(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptEnable_62( );
   }
   
   void uScript_GraphEvents_uScriptDisable_62(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptDisable_62( );
   }
   
   void uScript_GraphEvents_uScriptDestroy_62(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptDestroy_62( );
   }
   
   void Relay_KeyEvent_0()
   {
      if (true == CheckDebugBreak("b481740a-5d91-4421-98aa-8e311cc447fa", "Input_Events", Relay_KeyEvent_0)) return; 
      Relay_In_1();
   }
   
   void Relay_In_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         uScriptDebug.Log( "[Detox.ScriptEditor.LogicNode] space pressed", uScriptDebug.Type.Message);
         if (true == CheckDebugBreak("3ee578ed-4786-4017-a86d-56613cbcaef7", "Input_Events_Filter", Relay_In_1)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1.In(logic_uScriptAct_OnInputEventFilter_KeyCode_1);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1.KeyDown;
         bool test_1 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1.KeyUp;
         
         if ( test_0 == true )
         {
            Relay_In_32();
         }
         if ( test_1 == true )
         {
            Relay_In_39();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_uScriptEnable_5()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("ee962a1b-80a6-4aeb-bf09-47a173970ac6", "Graph_Events", Relay_uScriptEnable_5)) return; 
         Relay_In_7();
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Graph Events.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_uScriptDisable_5()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("ee962a1b-80a6-4aeb-bf09-47a173970ac6", "Graph_Events", Relay_uScriptDisable_5)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Graph Events.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_uScriptDestroy_5()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("ee962a1b-80a6-4aeb-bf09-47a173970ac6", "Graph_Events", Relay_uScriptDestroy_5)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Graph Events.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_7()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("31c8b5c7-6c7b-451f-9705-8e0d886f1787", "Multiply_Float", Relay_In_7)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_7 = local_8_System_Single;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_7 = MaxJumpHeight;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_7.In(logic_uScriptAct_MultiplyFloat_v2_A_7, logic_uScriptAct_MultiplyFloat_v2_B_7, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_7, out logic_uScriptAct_MultiplyFloat_v2_IntResult_7);
         local_10_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_7;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_7.Out;
         
         if ( test_0 == true )
         {
            Relay_In_11();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("e0ca9ca4-4212-4380-9b4f-8980dc097173", "Divide_Float", Relay_In_9)) return; 
         {
            {
               logic_uScriptAct_DivideFloat_A_9 = local_10_System_Single;
               
            }
            {
               logic_uScriptAct_DivideFloat_B_9 = local_12_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_DivideFloat_uScriptAct_DivideFloat_9.In(logic_uScriptAct_DivideFloat_A_9, logic_uScriptAct_DivideFloat_B_9, out logic_uScriptAct_DivideFloat_FloatResult_9, out logic_uScriptAct_DivideFloat_IntResult_9);
         local_13_System_Single = logic_uScriptAct_DivideFloat_FloatResult_9;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_DivideFloat_uScriptAct_DivideFloat_9.Out;
         
         if ( test_0 == true )
         {
            Relay_In_14();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Divide Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_11()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("5864e52d-ddff-45b6-b2f5-07d9dd59b22b", "Multiply_Float", Relay_In_11)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_11 = TimeToApex;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_11 = TimeToApex;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_11.In(logic_uScriptAct_MultiplyFloat_v2_A_11, logic_uScriptAct_MultiplyFloat_v2_B_11, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_11, out logic_uScriptAct_MultiplyFloat_v2_IntResult_11);
         local_12_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_11;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_11.Out;
         
         if ( test_0 == true )
         {
            Relay_In_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_14()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("6755e0ae-2c81-4a39-a882-23b7acc332b8", "Multiply_Float", Relay_In_14)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_14 = local_13_System_Single;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_14 = local_15_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_14.In(logic_uScriptAct_MultiplyFloat_v2_A_14, logic_uScriptAct_MultiplyFloat_v2_B_14, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_14, out logic_uScriptAct_MultiplyFloat_v2_IntResult_14);
         local_Gravity_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_14;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_14.Out;
         
         if ( test_0 == true )
         {
            Relay_Abs_17();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Abs_17()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("b5b8eb77-7949-4b8f-a45a-2ea1fdffaac9", "System_Math", Relay_Abs_17)) return; 
         {
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_value_17 = local_Gravity_System_Single;
               
            }
            {
            }
         }
         method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_17 = System.Math.Abs(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_value_17);
         local_AbsGravity_System_Single = method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_17;
         Relay_In_19();
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at System.Math.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_19()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("84e70a80-9a04-4a14-92b1-2bd46325148c", "Multiply_Float", Relay_In_19)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_19 = local_AbsGravity_System_Single;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_19 = TimeToApex;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_19.In(logic_uScriptAct_MultiplyFloat_v2_A_19, logic_uScriptAct_MultiplyFloat_v2_B_19, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_19, out logic_uScriptAct_MultiplyFloat_v2_IntResult_19);
         local_MaxJumpHeightVelocity_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_19;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_19.Out;
         
         if ( test_0 == true )
         {
            Relay_In_22();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_22()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("543990f0-d8a9-4c12-9bd4-fea5c7c8d7a6", "Multiply_Float", Relay_In_22)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_22 = local_AbsGravity_System_Single;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_22 = local_23_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_22.In(logic_uScriptAct_MultiplyFloat_v2_A_22, logic_uScriptAct_MultiplyFloat_v2_B_22, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_22, out logic_uScriptAct_MultiplyFloat_v2_IntResult_22);
         local_25_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_22;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_22.Out;
         
         if ( test_0 == true )
         {
            Relay_In_24();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_24()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("44ed7500-8ea2-4e0f-b562-7f255a105e17", "Multiply_Float", Relay_In_24)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_24 = local_25_System_Single;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_24 = MinJumpHeight;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_24.In(logic_uScriptAct_MultiplyFloat_v2_A_24, logic_uScriptAct_MultiplyFloat_v2_B_24, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_24, out logic_uScriptAct_MultiplyFloat_v2_IntResult_24);
         local_26_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_24;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_24.Out;
         
         if ( test_0 == true )
         {
            Relay_Sqrt_27();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Sqrt_27()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("02728919-a4ba-4d16-9f1a-00f43fa024ca", "UnityEngine_Mathf", Relay_Sqrt_27)) return; 
         {
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_f_27 = local_26_System_Single;
               
            }
            {
            }
         }
         method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_27 = UnityEngine.Mathf.Sqrt(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_f_27);
         local_MinJumpHeightVelocity_System_Single = method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_27;
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at UnityEngine.Mathf.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_31()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("30c2705c-2690-4ee4-929b-58ab56d049a6", "Replace_Components__Vector2_", Relay_In_31)) return; 
         {
            {
               logic_uScriptAct_ReplaceComponentsVector2_Target_31 = local_33_UnityEngine_Vector2;
               
            }
            {
            }
            {
            }
            {
               logic_uScriptAct_ReplaceComponentsVector2_Y_31 = local_MaxJumpHeightVelocity_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_31.In(logic_uScriptAct_ReplaceComponentsVector2_Target_31, logic_uScriptAct_ReplaceComponentsVector2_X_31, logic_uScriptAct_ReplaceComponentsVector2_IgnoreX_31, logic_uScriptAct_ReplaceComponentsVector2_Y_31, logic_uScriptAct_ReplaceComponentsVector2_IgnoreY_31, out logic_uScriptAct_ReplaceComponentsVector2_Output_31);
         local_33_UnityEngine_Vector2 = logic_uScriptAct_ReplaceComponentsVector2_Output_31;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_31.Out;
         
         if ( test_0 == true )
         {
            Relay_In_55();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Replace Components (Vector2).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_32()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("18155ce2-aee7-4e59-ac19-4a1edc420e4e", "Get_Rigidbody_Velocity__2D_", Relay_In_32)) return; 
         {
            {
               logic_uScriptAct_GetRigidbodyVelocity2D_Target_32 = owner_Connection_30;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetRigidbodyVelocity2D_uScriptAct_GetRigidbodyVelocity2D_32.In(logic_uScriptAct_GetRigidbodyVelocity2D_Target_32, out logic_uScriptAct_GetRigidbodyVelocity2D_Velocity_32, out logic_uScriptAct_GetRigidbodyVelocity2D_AngularVelocity_32, out logic_uScriptAct_GetRigidbodyVelocity2D_Magnitude_32);
         local_33_UnityEngine_Vector2 = logic_uScriptAct_GetRigidbodyVelocity2D_Velocity_32;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetRigidbodyVelocity2D_uScriptAct_GetRigidbodyVelocity2D_32.Out;
         
         if ( test_0 == true )
         {
            Relay_In_31();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Get Rigidbody Velocity (2D).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_34()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("509b7094-057f-4f39-991f-befa2f960ff3", "Compare_Float", Relay_In_34)) return; 
         {
            {
               logic_uScriptCon_CompareFloat_A_34 = local_41_System_Single;
               
            }
            {
               logic_uScriptCon_CompareFloat_B_34 = local_MinJumpHeightVelocity_System_Single;
               
            }
         }
         logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_34.In(logic_uScriptCon_CompareFloat_A_34, logic_uScriptCon_CompareFloat_B_34);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_34.GreaterThan;
         
         if ( test_0 == true )
         {
            Relay_In_35();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Compare Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_35()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("5a41a2f4-abf3-4445-93f3-7942c64938ee", "Replace_Components__Vector2_", Relay_In_35)) return; 
         {
            {
               logic_uScriptAct_ReplaceComponentsVector2_Target_35 = local_36_UnityEngine_Vector2;
               
            }
            {
            }
            {
            }
            {
               logic_uScriptAct_ReplaceComponentsVector2_Y_35 = local_MinJumpHeightVelocity_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_35.In(logic_uScriptAct_ReplaceComponentsVector2_Target_35, logic_uScriptAct_ReplaceComponentsVector2_X_35, logic_uScriptAct_ReplaceComponentsVector2_IgnoreX_35, logic_uScriptAct_ReplaceComponentsVector2_Y_35, logic_uScriptAct_ReplaceComponentsVector2_IgnoreY_35, out logic_uScriptAct_ReplaceComponentsVector2_Output_35);
         local_36_UnityEngine_Vector2 = logic_uScriptAct_ReplaceComponentsVector2_Output_35;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_35.Out;
         
         if ( test_0 == true )
         {
            Relay_In_42();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Replace Components (Vector2).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_38()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("61593a01-f6c7-43ae-9f28-7be31096de42", "Get_Components__Vector2_", Relay_In_38)) return; 
         {
            {
               logic_uScriptAct_GetComponentsVector2_InputVector2_38 = local_40_UnityEngine_Vector2;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetComponentsVector2_uScriptAct_GetComponentsVector2_38.In(logic_uScriptAct_GetComponentsVector2_InputVector2_38, out logic_uScriptAct_GetComponentsVector2_X_38, out logic_uScriptAct_GetComponentsVector2_Y_38);
         local_41_System_Single = logic_uScriptAct_GetComponentsVector2_Y_38;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetComponentsVector2_uScriptAct_GetComponentsVector2_38.Out;
         
         if ( test_0 == true )
         {
            Relay_In_34();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Get Components (Vector2).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_39()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("196fe727-67cb-4cbe-bf1a-d21d989cd1d7", "Get_Rigidbody_Velocity__2D_", Relay_In_39)) return; 
         {
            {
               logic_uScriptAct_GetRigidbodyVelocity2D_Target_39 = owner_Connection_56;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetRigidbodyVelocity2D_uScriptAct_GetRigidbodyVelocity2D_39.In(logic_uScriptAct_GetRigidbodyVelocity2D_Target_39, out logic_uScriptAct_GetRigidbodyVelocity2D_Velocity_39, out logic_uScriptAct_GetRigidbodyVelocity2D_AngularVelocity_39, out logic_uScriptAct_GetRigidbodyVelocity2D_Magnitude_39);
         local_40_UnityEngine_Vector2 = logic_uScriptAct_GetRigidbodyVelocity2D_Velocity_39;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetRigidbodyVelocity2D_uScriptAct_GetRigidbodyVelocity2D_39.Out;
         
         if ( test_0 == true )
         {
            Relay_In_38();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Get Rigidbody Velocity (2D).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_42()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("2168328d-8148-4bae-826c-529b0ea97a88", "Set_Rigidbody2D_Velocity__2D_", Relay_In_42)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_SetRigidbodyVelocity2D_Target_42.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetRigidbodyVelocity2D_Target_42, index + 1);
               }
               logic_uScriptAct_SetRigidbodyVelocity2D_Target_42[ index++ ] = owner_Connection_57;
               
            }
            {
               logic_uScriptAct_SetRigidbodyVelocity2D_Velocity_42 = local_36_UnityEngine_Vector2;
               
            }
         }
         logic_uScriptAct_SetRigidbodyVelocity2D_uScriptAct_SetRigidbodyVelocity2D_42.In(logic_uScriptAct_SetRigidbodyVelocity2D_Target_42, logic_uScriptAct_SetRigidbodyVelocity2D_Velocity_42);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Set Rigidbody2D Velocity (2D).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnUpdate_43()
   {
      if (true == CheckDebugBreak("982f18b5-c044-4448-a890-ba45c7ace507", "Global_Update", Relay_OnUpdate_43)) return; 
      Relay_In_47();
   }
   
   void Relay_OnLateUpdate_43()
   {
      if (true == CheckDebugBreak("982f18b5-c044-4448-a890-ba45c7ace507", "Global_Update", Relay_OnLateUpdate_43)) return; 
   }
   
   void Relay_OnFixedUpdate_43()
   {
      if (true == CheckDebugBreak("982f18b5-c044-4448-a890-ba45c7ace507", "Global_Update", Relay_OnFixedUpdate_43)) return; 
   }
   
   void Relay_In_45()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("6f2416fc-bc10-4c16-aaab-91a5384772dd", "Get_Components__Vector2_", Relay_In_45)) return; 
         {
            {
               logic_uScriptAct_GetComponentsVector2_InputVector2_45 = local_46_UnityEngine_Vector2;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetComponentsVector2_uScriptAct_GetComponentsVector2_45.In(logic_uScriptAct_GetComponentsVector2_InputVector2_45, out logic_uScriptAct_GetComponentsVector2_X_45, out logic_uScriptAct_GetComponentsVector2_Y_45);
         local_93_System_Single = logic_uScriptAct_GetComponentsVector2_X_45;
         local_48_System_Single = logic_uScriptAct_GetComponentsVector2_Y_45;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetComponentsVector2_uScriptAct_GetComponentsVector2_45.Out;
         
         if ( test_0 == true )
         {
            Relay_In_49();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Get Components (Vector2).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_47()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("246babac-2771-464f-8987-44b1af028fda", "Get_Rigidbody_Velocity__2D_", Relay_In_47)) return; 
         {
            {
               logic_uScriptAct_GetRigidbodyVelocity2D_Target_47 = owner_Connection_44;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetRigidbodyVelocity2D_uScriptAct_GetRigidbodyVelocity2D_47.In(logic_uScriptAct_GetRigidbodyVelocity2D_Target_47, out logic_uScriptAct_GetRigidbodyVelocity2D_Velocity_47, out logic_uScriptAct_GetRigidbodyVelocity2D_AngularVelocity_47, out logic_uScriptAct_GetRigidbodyVelocity2D_Magnitude_47);
         local_46_UnityEngine_Vector2 = logic_uScriptAct_GetRigidbodyVelocity2D_Velocity_47;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetRigidbodyVelocity2D_uScriptAct_GetRigidbodyVelocity2D_47.Out;
         
         if ( test_0 == true )
         {
            Relay_In_45();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Get Rigidbody Velocity (2D).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_49()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("a9d10847-aa69-49fb-b133-36ef780a6c04", "Add_Float", Relay_In_49)) return; 
         {
            {
               logic_uScriptAct_AddFloat_v2_A_49 = local_48_System_Single;
               
            }
            {
               logic_uScriptAct_AddFloat_v2_B_49 = local_Gravity_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_49.In(logic_uScriptAct_AddFloat_v2_A_49, logic_uScriptAct_AddFloat_v2_B_49, out logic_uScriptAct_AddFloat_v2_FloatResult_49, out logic_uScriptAct_AddFloat_v2_IntResult_49);
         local_50_System_Single = logic_uScriptAct_AddFloat_v2_FloatResult_49;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_AddFloat_v2_uScriptAct_AddFloat_v2_49.Out;
         
         if ( test_0 == true )
         {
            Relay_In_51();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Add Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_51()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("aa9ebf56-0664-47ba-b7ce-ab5b0714f99d", "Replace_Components__Vector2_", Relay_In_51)) return; 
         {
            {
               logic_uScriptAct_ReplaceComponentsVector2_Target_51 = local_52_UnityEngine_Vector2;
               
            }
            {
            }
            {
            }
            {
               logic_uScriptAct_ReplaceComponentsVector2_Y_51 = local_50_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_51.In(logic_uScriptAct_ReplaceComponentsVector2_Target_51, logic_uScriptAct_ReplaceComponentsVector2_X_51, logic_uScriptAct_ReplaceComponentsVector2_IgnoreX_51, logic_uScriptAct_ReplaceComponentsVector2_Y_51, logic_uScriptAct_ReplaceComponentsVector2_IgnoreY_51, out logic_uScriptAct_ReplaceComponentsVector2_Output_51);
         local_52_UnityEngine_Vector2 = logic_uScriptAct_ReplaceComponentsVector2_Output_51;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_51.Out;
         
         if ( test_0 == true )
         {
            Relay_In_53();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Replace Components (Vector2).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_53()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("ea6ec7f4-2a4f-4b56-a15a-178d929c7e7d", "Set_Rigidbody2D_Velocity__2D_", Relay_In_53)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_SetRigidbodyVelocity2D_Target_53.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetRigidbodyVelocity2D_Target_53, index + 1);
               }
               logic_uScriptAct_SetRigidbodyVelocity2D_Target_53[ index++ ] = owner_Connection_44;
               
            }
            {
               logic_uScriptAct_SetRigidbodyVelocity2D_Velocity_53 = local_52_UnityEngine_Vector2;
               
            }
         }
         logic_uScriptAct_SetRigidbodyVelocity2D_uScriptAct_SetRigidbodyVelocity2D_53.In(logic_uScriptAct_SetRigidbodyVelocity2D_Target_53, logic_uScriptAct_SetRigidbodyVelocity2D_Velocity_53);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Set Rigidbody2D Velocity (2D).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_55()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("b901478d-6024-40b3-9d8f-65c86ca3db88", "Set_Rigidbody2D_Velocity__2D_", Relay_In_55)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_SetRigidbodyVelocity2D_Target_55.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetRigidbodyVelocity2D_Target_55, index + 1);
               }
               logic_uScriptAct_SetRigidbodyVelocity2D_Target_55[ index++ ] = owner_Connection_30;
               
            }
            {
               logic_uScriptAct_SetRigidbodyVelocity2D_Velocity_55 = local_33_UnityEngine_Vector2;
               
            }
         }
         logic_uScriptAct_SetRigidbodyVelocity2D_uScriptAct_SetRigidbodyVelocity2D_55.In(logic_uScriptAct_SetRigidbodyVelocity2D_Target_55, logic_uScriptAct_SetRigidbodyVelocity2D_Velocity_55);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Set Rigidbody2D Velocity (2D).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnUpdate_58()
   {
      if (true == CheckDebugBreak("dcefa635-dffb-4dbe-a4bc-9231a8ec9767", "Global_Update", Relay_OnUpdate_58)) return; 
      Relay_In_59();
   }
   
   void Relay_OnLateUpdate_58()
   {
      if (true == CheckDebugBreak("dcefa635-dffb-4dbe-a4bc-9231a8ec9767", "Global_Update", Relay_OnLateUpdate_58)) return; 
   }
   
   void Relay_OnFixedUpdate_58()
   {
      if (true == CheckDebugBreak("dcefa635-dffb-4dbe-a4bc-9231a8ec9767", "Global_Update", Relay_OnFixedUpdate_58)) return; 
   }
   
   void Relay_In_59()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("a30abcfb-b6b4-49e8-a4bf-b89640bceaa8", "Raycast__2D_", Relay_In_59)) return; 
         {
            {
               logic_uScriptAct_Raycast2D_Start_59 = local_raycastpoint_transform_UnityEngine_Transform;
               
            }
            {
               logic_uScriptAct_Raycast2D_Direction_59 = local_64_UnityEngine_Vector2;
               
            }
            {
               logic_uScriptAct_Raycast2D_Distance_59 = local_75_System_Single;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_Raycast2D_uScriptAct_Raycast2D_59.In(logic_uScriptAct_Raycast2D_Start_59, logic_uScriptAct_Raycast2D_Direction_59, logic_uScriptAct_Raycast2D_Distance_59, logic_uScriptAct_Raycast2D_layerMask_59, logic_uScriptAct_Raycast2D_include_59, logic_uScriptAct_Raycast2D_showRay_59, out logic_uScriptAct_Raycast2D_HitObject_59, out logic_uScriptAct_Raycast2D_HitLocation_59, out logic_uScriptAct_Raycast2D_HitNormal_59);
         local_Hit_Object_UnityEngine_GameObject = logic_uScriptAct_Raycast2D_HitObject_59;
         {
            //if our game object reference was changed then we need to reset event listeners
            if ( local_Hit_Object_UnityEngine_GameObject_previous != local_Hit_Object_UnityEngine_GameObject || false == m_RegisteredForEvents )
            {
               //tear down old listeners
               
               local_Hit_Object_UnityEngine_GameObject_previous = local_Hit_Object_UnityEngine_GameObject;
               
               //setup new listeners
            }
         }
         local_66_UnityEngine_Vector3 = logic_uScriptAct_Raycast2D_HitLocation_59;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_Raycast2D_uScriptAct_Raycast2D_59.NotObstructed;
         bool test_1 = logic_uScriptAct_Raycast2D_uScriptAct_Raycast2D_59.Obstructed;
         
         if ( test_0 == true )
         {
            Relay_False_79();
         }
         if ( test_1 == true )
         {
            Relay_In_71();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Raycast (2D).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_61()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("4eb11c5c-ed64-4016-9f7d-27de0f86e766", "Get_Transform", Relay_In_61)) return; 
         {
            {
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( RaycastPoint_previous != RaycastPoint || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     RaycastPoint_previous = RaycastPoint;
                     
                     //setup new listeners
                  }
               }
               logic_uScriptAct_GetTransform_Target_61 = RaycastPoint;
               
            }
            {
            }
         }
         logic_uScriptAct_GetTransform_uScriptAct_GetTransform_61.In(logic_uScriptAct_GetTransform_Target_61, out logic_uScriptAct_GetTransform_targetTransform_61);
         local_raycastpoint_transform_UnityEngine_Transform = logic_uScriptAct_GetTransform_targetTransform_61;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Get Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_uScriptEnable_62()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("5216bd02-3d04-4f25-b2e2-6a7056f5cd21", "Graph_Events", Relay_uScriptEnable_62)) return; 
         Relay_In_61();
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Graph Events.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_uScriptDisable_62()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("5216bd02-3d04-4f25-b2e2-6a7056f5cd21", "Graph_Events", Relay_uScriptDisable_62)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Graph Events.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_uScriptDestroy_62()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("5216bd02-3d04-4f25-b2e2-6a7056f5cd21", "Graph_Events", Relay_uScriptDestroy_62)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Graph Events.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_67()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("b9b6ed91-6a6e-4800-9c7f-3c2a631bedca", "Get_Vector2_Distance", Relay_In_67)) return; 
         {
            {
               logic_uScriptAct_GetVector2Distance_A_67 = local_hit_location_V2_UnityEngine_Vector2;
               
            }
            {
               logic_uScriptAct_GetVector2Distance_B_67 = local_68_UnityEngine_Vector2;
               
            }
            {
            }
         }
         logic_uScriptAct_GetVector2Distance_uScriptAct_GetVector2Distance_67.In(logic_uScriptAct_GetVector2Distance_A_67, logic_uScriptAct_GetVector2Distance_B_67, out logic_uScriptAct_GetVector2Distance_Distance_67);
         local_Grounding_distance_System_Single = logic_uScriptAct_GetVector2Distance_Distance_67;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Get Vector2 Distance.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_70()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("3240473f-348c-4f96-9bc1-93eec92603a7", "Replace_Components__Vector2_", Relay_In_70)) return; 
         {
            {
               logic_uScriptAct_ReplaceComponentsVector2_Target_70 = local_hit_location_V2_UnityEngine_Vector2;
               
            }
            {
               logic_uScriptAct_ReplaceComponentsVector2_X_70 = local_72_System_Single;
               
            }
            {
            }
            {
               logic_uScriptAct_ReplaceComponentsVector2_Y_70 = local_73_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_70.In(logic_uScriptAct_ReplaceComponentsVector2_Target_70, logic_uScriptAct_ReplaceComponentsVector2_X_70, logic_uScriptAct_ReplaceComponentsVector2_IgnoreX_70, logic_uScriptAct_ReplaceComponentsVector2_Y_70, logic_uScriptAct_ReplaceComponentsVector2_IgnoreY_70, out logic_uScriptAct_ReplaceComponentsVector2_Output_70);
         local_hit_location_V2_UnityEngine_Vector2 = logic_uScriptAct_ReplaceComponentsVector2_Output_70;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_70.Out;
         
         if ( test_0 == true )
         {
            Relay_In_80();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Replace Components (Vector2).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_71()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("76264219-9672-4c24-9f6a-49516105649e", "Get_Components__Vector3_", Relay_In_71)) return; 
         {
            {
               logic_uScriptAct_GetComponentsVector3_InputVector3_71 = local_66_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_71.In(logic_uScriptAct_GetComponentsVector3_InputVector3_71, out logic_uScriptAct_GetComponentsVector3_X_71, out logic_uScriptAct_GetComponentsVector3_Y_71, out logic_uScriptAct_GetComponentsVector3_Z_71);
         local_72_System_Single = logic_uScriptAct_GetComponentsVector3_X_71;
         local_73_System_Single = logic_uScriptAct_GetComponentsVector3_Y_71;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_71.Out;
         
         if ( test_0 == true )
         {
            Relay_In_70();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Get Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_76()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("1ad31e12-6242-4aa9-a82a-933ba414e3d6", "Compare_Float", Relay_In_76)) return; 
         {
            {
               logic_uScriptCon_CompareFloat_A_76 = local_Grounding_distance_System_Single;
               
            }
            {
               logic_uScriptCon_CompareFloat_B_76 = local_77_System_Single;
               
            }
         }
         logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_76.In(logic_uScriptCon_CompareFloat_A_76, logic_uScriptCon_CompareFloat_B_76);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_76.GreaterThanOrEqualTo;
         bool test_1 = logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_76.LessThan;
         
         if ( test_0 == true )
         {
            Relay_False_79();
         }
         if ( test_1 == true )
         {
            Relay_True_79();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Compare Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_True_79()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("cb8e26ec-3e0c-4533-8199-e7dd85b1cafc", "Set_Bool", Relay_True_79)) return; 
         {
            {
            }
         }
         logic_uScriptAct_SetBool_uScriptAct_SetBool_79.True(out logic_uScriptAct_SetBool_Target_79);
         IsGrounded = logic_uScriptAct_SetBool_Target_79;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Set Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_False_79()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("cb8e26ec-3e0c-4533-8199-e7dd85b1cafc", "Set_Bool", Relay_False_79)) return; 
         {
            {
            }
         }
         logic_uScriptAct_SetBool_uScriptAct_SetBool_79.False(out logic_uScriptAct_SetBool_Target_79);
         IsGrounded = logic_uScriptAct_SetBool_Target_79;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Set Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_80()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("460dd236-66a1-44c2-93bd-4c0c95f6f5ff", "Get_Position_From_Transform", Relay_In_80)) return; 
         {
            {
               logic_uScriptAct_GetPositionFromTransform_target_80 = local_raycastpoint_transform_UnityEngine_Transform;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_80.In(logic_uScriptAct_GetPositionFromTransform_target_80, logic_uScriptAct_GetPositionFromTransform_getLocal_80, out logic_uScriptAct_GetPositionFromTransform_position_80, out logic_uScriptAct_GetPositionFromTransform_forward_80, out logic_uScriptAct_GetPositionFromTransform_right_80, out logic_uScriptAct_GetPositionFromTransform_up_80, out logic_uScriptAct_GetPositionFromTransform_worldMatrix_80);
         local_81_UnityEngine_Vector3 = logic_uScriptAct_GetPositionFromTransform_position_80;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_80.Out;
         
         if ( test_0 == true )
         {
            Relay_In_84();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Get Position From Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_82()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("064da643-fa96-4671-8d5e-e000d106836a", "Replace_Components__Vector2_", Relay_In_82)) return; 
         {
            {
               logic_uScriptAct_ReplaceComponentsVector2_Target_82 = local_68_UnityEngine_Vector2;
               
            }
            {
               logic_uScriptAct_ReplaceComponentsVector2_X_82 = local_85_System_Single;
               
            }
            {
            }
            {
               logic_uScriptAct_ReplaceComponentsVector2_Y_82 = local_83_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_82.In(logic_uScriptAct_ReplaceComponentsVector2_Target_82, logic_uScriptAct_ReplaceComponentsVector2_X_82, logic_uScriptAct_ReplaceComponentsVector2_IgnoreX_82, logic_uScriptAct_ReplaceComponentsVector2_Y_82, logic_uScriptAct_ReplaceComponentsVector2_IgnoreY_82, out logic_uScriptAct_ReplaceComponentsVector2_Output_82);
         local_68_UnityEngine_Vector2 = logic_uScriptAct_ReplaceComponentsVector2_Output_82;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_82.Out;
         
         if ( test_0 == true )
         {
            Relay_In_67();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Replace Components (Vector2).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_84()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("5abe7d34-a359-4089-94c2-3bbde84730c3", "Get_Components__Vector3_", Relay_In_84)) return; 
         {
            {
               logic_uScriptAct_GetComponentsVector3_InputVector3_84 = local_81_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_84.In(logic_uScriptAct_GetComponentsVector3_InputVector3_84, out logic_uScriptAct_GetComponentsVector3_X_84, out logic_uScriptAct_GetComponentsVector3_Y_84, out logic_uScriptAct_GetComponentsVector3_Z_84);
         local_85_System_Single = logic_uScriptAct_GetComponentsVector3_X_84;
         local_83_System_Single = logic_uScriptAct_GetComponentsVector3_Y_84;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_84.Out;
         
         if ( test_0 == true )
         {
            Relay_In_82();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Get Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnUpdate_86()
   {
      if (true == CheckDebugBreak("f9844c67-116f-42ab-853b-238f542d7cd4", "Global_Update", Relay_OnUpdate_86)) return; 
      Relay_In_76();
   }
   
   void Relay_OnLateUpdate_86()
   {
      if (true == CheckDebugBreak("f9844c67-116f-42ab-853b-238f542d7cd4", "Global_Update", Relay_OnLateUpdate_86)) return; 
   }
   
   void Relay_OnFixedUpdate_86()
   {
      if (true == CheckDebugBreak("f9844c67-116f-42ab-853b-238f542d7cd4", "Global_Update", Relay_OnFixedUpdate_86)) return; 
   }
   
   void Relay_In_89()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("bf3244b4-7acc-4ecc-b5af-b6ea7f701b63", "Compare_Bool", Relay_In_89)) return; 
         {
            {
               logic_uScriptCon_CompareBool_Bool_89 = IsGrounded;
               
            }
         }
         logic_uScriptCon_CompareBool_uScriptCon_CompareBool_89.In(logic_uScriptCon_CompareBool_Bool_89);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_89.True;
         
         if ( test_0 == true )
         {
            Relay_In_90();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Compare Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_90()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("67506524-4cda-4f47-a8f6-29095b99e2b1", "Set_Rigidbody2D_Velocity__2D_", Relay_In_90)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_SetRigidbodyVelocity2D_Target_90.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetRigidbodyVelocity2D_Target_90, index + 1);
               }
               logic_uScriptAct_SetRigidbodyVelocity2D_Target_90[ index++ ] = owner_Connection_44;
               
            }
            {
               logic_uScriptAct_SetRigidbodyVelocity2D_Velocity_90 = local_91_UnityEngine_Vector2;
               
            }
         }
         logic_uScriptAct_SetRigidbodyVelocity2D_uScriptAct_SetRigidbodyVelocity2D_90.In(logic_uScriptAct_SetRigidbodyVelocity2D_Target_90, logic_uScriptAct_SetRigidbodyVelocity2D_Velocity_90);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Set Rigidbody2D Velocity (2D).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnUpdate_92()
   {
      if (true == CheckDebugBreak("e00aa01e-1b9a-4e07-9680-b6632a293f02", "Global_Update", Relay_OnUpdate_92)) return; 
      Relay_In_89();
      Relay_In_94();
   }
   
   void Relay_OnLateUpdate_92()
   {
      if (true == CheckDebugBreak("e00aa01e-1b9a-4e07-9680-b6632a293f02", "Global_Update", Relay_OnLateUpdate_92)) return; 
   }
   
   void Relay_OnFixedUpdate_92()
   {
      if (true == CheckDebugBreak("e00aa01e-1b9a-4e07-9680-b6632a293f02", "Global_Update", Relay_OnFixedUpdate_92)) return; 
   }
   
   void Relay_In_94()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("feb2c560-7cc5-4d67-9ea7-4abb77247b5d", "Replace_Components__Vector2_", Relay_In_94)) return; 
         {
            {
               logic_uScriptAct_ReplaceComponentsVector2_Target_94 = local_91_UnityEngine_Vector2;
               
            }
            {
               logic_uScriptAct_ReplaceComponentsVector2_X_94 = local_93_System_Single;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_94.In(logic_uScriptAct_ReplaceComponentsVector2_Target_94, logic_uScriptAct_ReplaceComponentsVector2_X_94, logic_uScriptAct_ReplaceComponentsVector2_IgnoreX_94, logic_uScriptAct_ReplaceComponentsVector2_Y_94, logic_uScriptAct_ReplaceComponentsVector2_IgnoreY_94, out logic_uScriptAct_ReplaceComponentsVector2_Output_94);
         local_91_UnityEngine_Vector2 = logic_uScriptAct_ReplaceComponentsVector2_Output_94;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript VariableJump.uscript at Replace Components (Vector2).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:MinJumpHeight", MinJumpHeight);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "d98a99ec-f38f-41f1-a7a0-01918982d998", MinJumpHeight);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:MaxJumpHeight", MaxJumpHeight);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "c78bcfd9-9dd0-4ab3-95a3-34e7bd101d16", MaxJumpHeight);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:TimeToApex", TimeToApex);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "f6c1b20b-6ab1-4e49-8f7b-f2c69ac6ec77", TimeToApex);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:Gravity", local_Gravity_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "c8930e85-7d7d-4932-b7db-4745e1199ab1", local_Gravity_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:8", local_8_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "fc70402c-1df6-4579-891f-4a5386c3f4df", local_8_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:10", local_10_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "c94dacb0-25e2-43da-8773-cf5b592a0cef", local_10_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:12", local_12_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "c09eceb0-1d90-458e-91b4-f91f18ce8a3f", local_12_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:13", local_13_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "2c18cd59-a97b-4049-b3b2-bca4ef20cd99", local_13_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:15", local_15_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "84914db0-2afe-41ee-aa4f-ab6c1c2c7216", local_15_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:AbsGravity", local_AbsGravity_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "32baef7b-cc37-495a-b5fb-7de5c551a487", local_AbsGravity_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:MaxJumpHeightVelocity", local_MaxJumpHeightVelocity_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "a9c8bb92-26d3-4fbb-bf7d-3a96ca8dbedb", local_MaxJumpHeightVelocity_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:23", local_23_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "330ee094-f92b-4293-94d5-d09d3284feb7", local_23_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:25", local_25_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "ae71cee1-45f9-41cd-bfe6-981210c12bc0", local_25_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:26", local_26_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "be05a708-1184-46e9-a05d-37af816ca562", local_26_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:MinJumpHeightVelocity", local_MinJumpHeightVelocity_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "6ce45dc1-57ca-4418-89a1-9d4a4c39c6fb", local_MinJumpHeightVelocity_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:33", local_33_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "af525a05-7d6e-4c92-b469-99562bdc17b5", local_33_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:36", local_36_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "bd936c63-a194-40a0-a0ac-b54a556bc1bc", local_36_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:40", local_40_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "84cd9757-5e15-4221-a656-aba8a40cc442", local_40_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:41", local_41_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "d413bd37-2f8d-479a-a58d-8b8c7d0f666b", local_41_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:46", local_46_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "9b5286d3-86fe-47c5-90e8-ef71faf10f2e", local_46_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:48", local_48_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "8a9ef236-827e-4bb4-b8c2-16c9458934cf", local_48_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:50", local_50_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "b712733c-fc78-42a4-8c93-455a953c427f", local_50_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:52", local_52_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "ebd4e862-9982-4c75-bc7d-fc292f495550", local_52_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:RaycastPoint", RaycastPoint);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "15501e0f-e659-45b1-a53d-6b2f5f52a64d", RaycastPoint);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:raycastpoint transform", local_raycastpoint_transform_UnityEngine_Transform);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "9f0ce560-601b-4d24-a44f-ffa45762e9f9", local_raycastpoint_transform_UnityEngine_Transform);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:64", local_64_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "9bbb3c90-6a29-4405-aab5-a1975c6ce677", local_64_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:Hit Object", local_Hit_Object_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "7086ea3d-7483-424c-8875-792d93e1f3af", local_Hit_Object_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:66", local_66_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "036dec29-2d5a-49a9-b955-dbe7247a9f43", local_66_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:68", local_68_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "623bee8a-a6ca-4c93-8d32-cdefec898374", local_68_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:hit location V2", local_hit_location_V2_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "1c68c09b-be0a-443d-a43b-0fa2dd893b6a", local_hit_location_V2_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:72", local_72_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "ead65ef9-4b1f-4513-b462-0e5305bc6e45", local_72_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:73", local_73_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "c741af0b-82e4-40b6-a43f-0c86f7ae6496", local_73_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:Grounding distance", local_Grounding_distance_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "34f6c226-58a0-4147-87f7-b76105e3d15c", local_Grounding_distance_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:75", local_75_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "de0feb62-1c47-4c83-8bce-9ebee508c063", local_75_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:77", local_77_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "821c663e-3199-4081-ad80-0a1be5f080e5", local_77_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:IsGrounded", IsGrounded);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "44a1f4e2-7aec-4e53-a799-f528a623b184", IsGrounded);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:81", local_81_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "e23028c7-b27a-4780-a3d5-ede8300b28c0", local_81_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:83", local_83_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "601c9926-ae40-4d84-a9b4-2a523eb0bece", local_83_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:85", local_85_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "31fbd56f-c44d-41a3-a561-28d3e0427df8", local_85_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:91", local_91_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "5a145743-cdf4-4a56-8a9d-3a3a345d1cb5", local_91_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "VariableJump.uscript:93", local_93_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "d3a728e5-791c-49ae-a27e-15db6ca459e3", local_93_System_Single);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
