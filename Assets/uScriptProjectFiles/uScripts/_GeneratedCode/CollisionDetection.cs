//uScript Generated Code - Build 1.0.3072
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class CollisionDetection : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_3_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_3_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_5_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_5_UnityEngine_GameObject_previous = null;
   System.Boolean local_7_System_Boolean = (bool) true;
   [Multiline(3)]
   public System.String Player = "Player";
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_Log logic_uScriptAct_Log_uScriptAct_Log_1 = new uScriptAct_Log( );
   System.Object logic_uScriptAct_Log_Prefix_1 = "";
   System.Object[] logic_uScriptAct_Log_Target_1 = new System.Object[] {};
   System.Object logic_uScriptAct_Log_Postfix_1 = "";
   bool logic_uScriptAct_Log_Out_1 = true;
   //pointer to script instanced logic node
   uScriptAct_Log logic_uScriptAct_Log_uScriptAct_Log_2 = new uScriptAct_Log( );
   System.Object logic_uScriptAct_Log_Prefix_2 = "";
   System.Object[] logic_uScriptAct_Log_Target_2 = new System.Object[] {};
   System.Object logic_uScriptAct_Log_Postfix_2 = "";
   bool logic_uScriptAct_Log_Out_2 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_8 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_8 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_8 = true;
   bool logic_uScriptCon_CompareBool_False_8 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_GameObject_4 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   System.String method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_tag_6 = "";
   System.Boolean method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_6 = (bool) false;
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_3_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_3_UnityEngine_GameObject = GameObject.Find( "/Cube" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_3_UnityEngine_GameObject_previous != local_3_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_3_UnityEngine_GameObject_previous )
         {
            {
               uScript_Trigger_NoStay component = local_3_UnityEngine_GameObject_previous.GetComponent<uScript_Trigger_NoStay>();
               if ( null != component )
               {
                  component.OnEnterTrigger -= Instance_OnEnterTrigger_4;
                  component.OnExitTrigger -= Instance_OnExitTrigger_4;
               }
            }
         }
         
         local_3_UnityEngine_GameObject_previous = local_3_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_3_UnityEngine_GameObject )
         {
            {
               uScript_Trigger_NoStay component = local_3_UnityEngine_GameObject.GetComponent<uScript_Trigger_NoStay>();
               if ( null == component )
               {
                  component = local_3_UnityEngine_GameObject.AddComponent<uScript_Trigger_NoStay>();
               }
               if ( null != component )
               {
                  component.OnEnterTrigger += Instance_OnEnterTrigger_4;
                  component.OnExitTrigger += Instance_OnExitTrigger_4;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_5_UnityEngine_GameObject_previous != local_5_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_5_UnityEngine_GameObject_previous = local_5_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_3_UnityEngine_GameObject_previous != local_3_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_3_UnityEngine_GameObject_previous )
         {
            {
               uScript_Trigger_NoStay component = local_3_UnityEngine_GameObject_previous.GetComponent<uScript_Trigger_NoStay>();
               if ( null != component )
               {
                  component.OnEnterTrigger -= Instance_OnEnterTrigger_4;
                  component.OnExitTrigger -= Instance_OnExitTrigger_4;
               }
            }
         }
         
         local_3_UnityEngine_GameObject_previous = local_3_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_3_UnityEngine_GameObject )
         {
            {
               uScript_Trigger_NoStay component = local_3_UnityEngine_GameObject.GetComponent<uScript_Trigger_NoStay>();
               if ( null == component )
               {
                  component = local_3_UnityEngine_GameObject.AddComponent<uScript_Trigger_NoStay>();
               }
               if ( null != component )
               {
                  component.OnEnterTrigger += Instance_OnEnterTrigger_4;
                  component.OnExitTrigger += Instance_OnExitTrigger_4;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_5_UnityEngine_GameObject_previous != local_5_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_5_UnityEngine_GameObject_previous = local_5_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void SyncEventListeners( )
   {
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != local_3_UnityEngine_GameObject )
      {
         {
            uScript_Trigger_NoStay component = local_3_UnityEngine_GameObject.GetComponent<uScript_Trigger_NoStay>();
            if ( null != component )
            {
               component.OnEnterTrigger -= Instance_OnEnterTrigger_4;
               component.OnExitTrigger -= Instance_OnExitTrigger_4;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_Log_uScriptAct_Log_1.SetParent(g);
      logic_uScriptAct_Log_uScriptAct_Log_2.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_8.SetParent(g);
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnEnterTrigger_4(object o, uScript_Trigger_NoStay.TriggerEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_GameObject_4 = e.GameObject;
      //relay event to nodes
      Relay_OnEnterTrigger_4( );
   }
   
   void Instance_OnExitTrigger_4(object o, uScript_Trigger_NoStay.TriggerEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_GameObject_4 = e.GameObject;
      //relay event to nodes
      Relay_OnExitTrigger_4( );
   }
   
   void Relay_In_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         uScriptDebug.Log( "[Detox.ScriptEditor.LogicNode] Same", uScriptDebug.Type.Message);
         if (true == CheckDebugBreak("abf304fb-0b5c-4cee-a3d3-bd1864f6b530", "Log", Relay_In_1)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_Log_uScriptAct_Log_1.In(logic_uScriptAct_Log_Prefix_1, logic_uScriptAct_Log_Target_1, logic_uScriptAct_Log_Postfix_1);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CollisionDetection.uscript at Log.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         uScriptDebug.Log( "[Detox.ScriptEditor.LogicNode] Different", uScriptDebug.Type.Message);
         if (true == CheckDebugBreak("4cde8076-8318-455e-8f94-9fd2f0575c4a", "Log", Relay_In_2)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_Log_uScriptAct_Log_2.In(logic_uScriptAct_Log_Prefix_2, logic_uScriptAct_Log_Target_2, logic_uScriptAct_Log_Postfix_2);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CollisionDetection.uscript at Log.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnEnterTrigger_4()
   {
      if (true == CheckDebugBreak("972ef395-ce92-483a-97e8-f4f543878bf0", "Trigger_Event___No_Stay", Relay_OnEnterTrigger_4)) return; 
      local_5_UnityEngine_GameObject = event_UnityEngine_GameObject_GameObject_4;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_5_UnityEngine_GameObject_previous != local_5_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_5_UnityEngine_GameObject_previous = local_5_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_CompareTag_6();
   }
   
   void Relay_OnExitTrigger_4()
   {
      if (true == CheckDebugBreak("972ef395-ce92-483a-97e8-f4f543878bf0", "Trigger_Event___No_Stay", Relay_OnExitTrigger_4)) return; 
      local_5_UnityEngine_GameObject = event_UnityEngine_GameObject_GameObject_4;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_5_UnityEngine_GameObject_previous != local_5_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_5_UnityEngine_GameObject_previous = local_5_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
   }
   
   void Relay_CompareTag_6()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("f9cad7b4-3f79-4382-ae91-cc66e4956a6d", "UnityEngine_BoxCollider", Relay_CompareTag_6)) return; 
         {
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_tag_6 = Player;
               
            }
            {
            }
         }
         {
            UnityEngine.BoxCollider component;
            component = local_5_UnityEngine_GameObject.GetComponent<UnityEngine.BoxCollider>();
            if ( null != component )
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_6 = component.CompareTag(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_tag_6);
            }
         }
         local_7_System_Boolean = method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_6;
         Relay_In_8();
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CollisionDetection.uscript at UnityEngine.BoxCollider.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_8()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("e6fae1a3-c092-43fa-862f-a6d53eb79b10", "Compare_Bool", Relay_In_8)) return; 
         {
            {
               logic_uScriptCon_CompareBool_Bool_8 = local_7_System_Boolean;
               
            }
         }
         logic_uScriptCon_CompareBool_uScriptCon_CompareBool_8.In(logic_uScriptCon_CompareBool_Bool_8);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_8.True;
         bool test_1 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_8.False;
         
         if ( test_0 == true )
         {
            Relay_In_1();
         }
         if ( test_1 == true )
         {
            Relay_In_2();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CollisionDetection.uscript at Compare Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CollisionDetection.uscript:Player", Player);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "a6fe8f7f-da9a-43d7-9dff-611658abf040", Player);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CollisionDetection.uscript:3", local_3_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "e29fb4ab-85c8-4a59-9d9c-d5b7124640bf", local_3_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CollisionDetection.uscript:5", local_5_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "ced5573b-a294-4dc2-8e8e-4d9d764d4858", local_5_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CollisionDetection.uscript:7", local_7_System_Boolean);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "82560554-ce28-4917-a917-faae4765f122", local_7_System_Boolean);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
