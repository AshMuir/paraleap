//uScript Generated Code - Build 1.0.3072
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "0.1")]
public class PlayerController : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public System.Boolean Jump_Check = (bool) true;
   public System.Single Jump_Force = (float) 20;
   UnityEngine.Transform local_13_UnityEngine_Transform = default(UnityEngine.Transform);
   UnityEngine.Vector3 local_14_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_15_System_Single = (float) 0;
   System.Single local_17_System_Single = (float) 5;
   UnityEngine.Vector2 local_20_UnityEngine_Vector2 = new Vector2( (float)0, (float)0 );
   System.Single local_25_System_Single = (float) 0.1;
   System.Single local_26_System_Single = (float) 0;
   UnityEngine.GameObject local_34_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_34_UnityEngine_GameObject_previous = null;
   UnityEngine.Vector3 local_37_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_39_System_Single = (float) 0;
   System.Single local_40_System_Single = (float) 0;
   UnityEngine.Vector3 local_43_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_46_System_Single = (float) 0;
   UnityEngine.Vector3 local_47_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_49_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_50_System_Single = (float) 0;
   UnityEngine.Transform local_58_UnityEngine_Transform = default(UnityEngine.Transform);
   UnityEngine.Vector3 local_59_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_6_System_Single = (float) 0;
   System.Single local_62_System_Single = (float) 0;
   UnityEngine.Vector3 local_64_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.GameObject local_67_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_67_UnityEngine_GameObject_previous = null;
   System.Single local_68_System_Single = (float) 0;
   System.Single local_69_System_Single = (float) 0;
   System.Single local_7_System_Single = (float) 10;
   UnityEngine.GameObject local_Player_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_Player_UnityEngine_GameObject_previous = null;
   public System.Single Player_Move_Speed = (float) 1;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_27 = null;
   UnityEngine.GameObject owner_Connection_32 = null;
   UnityEngine.GameObject owner_Connection_42 = null;
   UnityEngine.GameObject owner_Connection_51 = null;
   UnityEngine.GameObject owner_Connection_53 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_2 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_2;
   bool logic_uScriptAct_SetBool_Out_2 = true;
   bool logic_uScriptAct_SetBool_SetTrue_2 = true;
   bool logic_uScriptAct_SetBool_SetFalse_2 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_4 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_4 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_4 = true;
   bool logic_uScriptCon_CompareBool_False_4 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_5 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_5 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_5;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_5;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_5 = true;
   //pointer to script instanced logic node
   uScriptAct_OnMultipleInputEventFilter logic_uScriptAct_OnMultipleInputEventFilter_uScriptAct_OnMultipleInputEventFilter_9 = new uScriptAct_OnMultipleInputEventFilter( );
   UnityEngine.KeyCode[] logic_uScriptAct_OnMultipleInputEventFilter_KeyCode_9 = new UnityEngine.KeyCode[] {UnityEngine.KeyCode.Space};
   bool logic_uScriptAct_OnMultipleInputEventFilter_KeyDown_9 = true;
   bool logic_uScriptAct_OnMultipleInputEventFilter_KeyHeld_9 = true;
   bool logic_uScriptAct_OnMultipleInputEventFilter_KeyUp_9 = true;
   //pointer to script instanced logic node
   uScriptAct_OnMultipleInputEventFilter logic_uScriptAct_OnMultipleInputEventFilter_uScriptAct_OnMultipleInputEventFilter_10 = new uScriptAct_OnMultipleInputEventFilter( );
   UnityEngine.KeyCode[] logic_uScriptAct_OnMultipleInputEventFilter_KeyCode_10 = new UnityEngine.KeyCode[] {UnityEngine.KeyCode.LeftControl};
   bool logic_uScriptAct_OnMultipleInputEventFilter_KeyDown_10 = true;
   bool logic_uScriptAct_OnMultipleInputEventFilter_KeyHeld_10 = true;
   bool logic_uScriptAct_OnMultipleInputEventFilter_KeyUp_10 = true;
   //pointer to script instanced logic node
   uScriptAct_GetComponentsVector3 logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_11 = new uScriptAct_GetComponentsVector3( );
   UnityEngine.Vector3 logic_uScriptAct_GetComponentsVector3_InputVector3_11 = new Vector3( );
   System.Single logic_uScriptAct_GetComponentsVector3_X_11;
   System.Single logic_uScriptAct_GetComponentsVector3_Y_11;
   System.Single logic_uScriptAct_GetComponentsVector3_Z_11;
   bool logic_uScriptAct_GetComponentsVector3_Out_11 = true;
   //pointer to script instanced logic node
   uScriptAct_GetPositionFromTransform logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_12 = new uScriptAct_GetPositionFromTransform( );
   UnityEngine.Transform logic_uScriptAct_GetPositionFromTransform_target_12 = default(UnityEngine.Transform);
   System.Boolean logic_uScriptAct_GetPositionFromTransform_getLocal_12 = (bool) false;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_position_12;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_forward_12;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_right_12;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_up_12;
   UnityEngine.Matrix4x4 logic_uScriptAct_GetPositionFromTransform_worldMatrix_12;
   bool logic_uScriptAct_GetPositionFromTransform_Out_12 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareFloat logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_16 = new uScriptCon_CompareFloat( );
   System.Single logic_uScriptCon_CompareFloat_A_16 = (float) 0;
   System.Single logic_uScriptCon_CompareFloat_B_16 = (float) 0;
   bool logic_uScriptCon_CompareFloat_GreaterThan_16 = true;
   bool logic_uScriptCon_CompareFloat_GreaterThanOrEqualTo_16 = true;
   bool logic_uScriptCon_CompareFloat_EqualTo_16 = true;
   bool logic_uScriptCon_CompareFloat_NotEqualTo_16 = true;
   bool logic_uScriptCon_CompareFloat_LessThanOrEqualTo_16 = true;
   bool logic_uScriptCon_CompareFloat_LessThan_16 = true;
   //pointer to script instanced logic node
   uScriptAct_AddForce2D logic_uScriptAct_AddForce2D_uScriptAct_AddForce2D_19 = new uScriptAct_AddForce2D( );
   UnityEngine.GameObject logic_uScriptAct_AddForce2D_Target_19 = default(UnityEngine.GameObject);
   UnityEngine.Vector2 logic_uScriptAct_AddForce2D_force_19 = new Vector2( );
   System.Single logic_uScriptAct_AddForce2D_scale_19 = (float) 0;
   bool logic_uScriptAct_AddForce2D_Out_19 = true;
   //pointer to script instanced logic node
   uScriptAct_ReplaceComponentsVector2 logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_21 = new uScriptAct_ReplaceComponentsVector2( );
   UnityEngine.Vector2 logic_uScriptAct_ReplaceComponentsVector2_Target_21 = new Vector2( );
   System.Single logic_uScriptAct_ReplaceComponentsVector2_X_21 = (float) 0;
   System.Boolean logic_uScriptAct_ReplaceComponentsVector2_IgnoreX_21 = (bool) false;
   System.Single logic_uScriptAct_ReplaceComponentsVector2_Y_21 = (float) 0;
   System.Boolean logic_uScriptAct_ReplaceComponentsVector2_IgnoreY_21 = (bool) false;
   UnityEngine.Vector2 logic_uScriptAct_ReplaceComponentsVector2_Output_21;
   bool logic_uScriptAct_ReplaceComponentsVector2_Out_21 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_22 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_22 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_22 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_22;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_22;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_22 = true;
   //pointer to script instanced logic node
   uScriptAct_SetLayer logic_uScriptAct_SetLayer_uScriptAct_SetLayer_31 = new uScriptAct_SetLayer( );
   UnityEngine.GameObject[] logic_uScriptAct_SetLayer_Target_31 = new UnityEngine.GameObject[] {};
   UnityEngine.LayerMask logic_uScriptAct_SetLayer_Layer_31 = 512;
   System.Boolean logic_uScriptAct_SetLayer_ApplyToChildren_31 = (bool) true;
   bool logic_uScriptAct_SetLayer_Out_31 = true;
   //pointer to script instanced logic node
   uScriptAct_SetComponentsVector3 logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_35 = new uScriptAct_SetComponentsVector3( );
   System.Single logic_uScriptAct_SetComponentsVector3_X_35 = (float) 0;
   System.Single logic_uScriptAct_SetComponentsVector3_Y_35 = (float) 0;
   System.Single logic_uScriptAct_SetComponentsVector3_Z_35 = (float) 10;
   UnityEngine.Vector3 logic_uScriptAct_SetComponentsVector3_OutputVector3_35;
   bool logic_uScriptAct_SetComponentsVector3_Out_35 = true;
   //pointer to script instanced logic node
   uScriptAct_GetPositionAndRotation logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_36 = new uScriptAct_GetPositionAndRotation( );
   UnityEngine.GameObject logic_uScriptAct_GetPositionAndRotation_Target_36 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptAct_GetPositionAndRotation_GetLocal_36 = (bool) false;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Position_36;
   UnityEngine.Quaternion logic_uScriptAct_GetPositionAndRotation_Rotation_36;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_EulerAngles_36;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Forward_36;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Up_36;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Right_36;
   bool logic_uScriptAct_GetPositionAndRotation_Out_36 = true;
   //pointer to script instanced logic node
   uScriptAct_GetComponentsVector3 logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_38 = new uScriptAct_GetComponentsVector3( );
   UnityEngine.Vector3 logic_uScriptAct_GetComponentsVector3_InputVector3_38 = new Vector3( );
   System.Single logic_uScriptAct_GetComponentsVector3_X_38;
   System.Single logic_uScriptAct_GetComponentsVector3_Y_38;
   System.Single logic_uScriptAct_GetComponentsVector3_Z_38;
   bool logic_uScriptAct_GetComponentsVector3_Out_38 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_41 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_41 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_41 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_41 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_41 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_41 = true;
   //pointer to script instanced logic node
   uScriptAct_GetComponentsVector3 logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_44 = new uScriptAct_GetComponentsVector3( );
   UnityEngine.Vector3 logic_uScriptAct_GetComponentsVector3_InputVector3_44 = new Vector3( );
   System.Single logic_uScriptAct_GetComponentsVector3_X_44;
   System.Single logic_uScriptAct_GetComponentsVector3_Y_44;
   System.Single logic_uScriptAct_GetComponentsVector3_Z_44;
   bool logic_uScriptAct_GetComponentsVector3_Out_44 = true;
   //pointer to script instanced logic node
   uScriptAct_GetPositionAndRotation logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_45 = new uScriptAct_GetPositionAndRotation( );
   UnityEngine.GameObject logic_uScriptAct_GetPositionAndRotation_Target_45 = default(UnityEngine.GameObject);
   System.Boolean logic_uScriptAct_GetPositionAndRotation_GetLocal_45 = (bool) false;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Position_45;
   UnityEngine.Quaternion logic_uScriptAct_GetPositionAndRotation_Rotation_45;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_EulerAngles_45;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Forward_45;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Up_45;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionAndRotation_Right_45;
   bool logic_uScriptAct_GetPositionAndRotation_Out_45 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_52 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_52 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_52 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_52 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_52 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_52 = true;
   //pointer to script instanced logic node
   uScriptAct_SetLayer logic_uScriptAct_SetLayer_uScriptAct_SetLayer_54 = new uScriptAct_SetLayer( );
   UnityEngine.GameObject[] logic_uScriptAct_SetLayer_Target_54 = new UnityEngine.GameObject[] {};
   UnityEngine.LayerMask logic_uScriptAct_SetLayer_Layer_54 = 256;
   System.Boolean logic_uScriptAct_SetLayer_ApplyToChildren_54 = (bool) true;
   bool logic_uScriptAct_SetLayer_Out_54 = true;
   //pointer to script instanced logic node
   uScriptAct_SetComponentsVector3 logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_55 = new uScriptAct_SetComponentsVector3( );
   System.Single logic_uScriptAct_SetComponentsVector3_X_55 = (float) 0;
   System.Single logic_uScriptAct_SetComponentsVector3_Y_55 = (float) 0;
   System.Single logic_uScriptAct_SetComponentsVector3_Z_55 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_SetComponentsVector3_OutputVector3_55;
   bool logic_uScriptAct_SetComponentsVector3_Out_55 = true;
   //pointer to script instanced logic node
   uScriptAct_GetPositionFromTransform logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_57 = new uScriptAct_GetPositionFromTransform( );
   UnityEngine.Transform logic_uScriptAct_GetPositionFromTransform_target_57 = default(UnityEngine.Transform);
   System.Boolean logic_uScriptAct_GetPositionFromTransform_getLocal_57 = (bool) false;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_position_57;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_forward_57;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_right_57;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_up_57;
   UnityEngine.Matrix4x4 logic_uScriptAct_GetPositionFromTransform_worldMatrix_57;
   bool logic_uScriptAct_GetPositionFromTransform_Out_57 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareFloat logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_60 = new uScriptCon_CompareFloat( );
   System.Single logic_uScriptCon_CompareFloat_A_60 = (float) 0;
   System.Single logic_uScriptCon_CompareFloat_B_60 = (float) 0;
   bool logic_uScriptCon_CompareFloat_GreaterThan_60 = true;
   bool logic_uScriptCon_CompareFloat_GreaterThanOrEqualTo_60 = true;
   bool logic_uScriptCon_CompareFloat_EqualTo_60 = true;
   bool logic_uScriptCon_CompareFloat_NotEqualTo_60 = true;
   bool logic_uScriptCon_CompareFloat_LessThanOrEqualTo_60 = true;
   bool logic_uScriptCon_CompareFloat_LessThan_60 = true;
   //pointer to script instanced logic node
   uScriptAct_GetComponentsVector3 logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_61 = new uScriptAct_GetComponentsVector3( );
   UnityEngine.Vector3 logic_uScriptAct_GetComponentsVector3_InputVector3_61 = new Vector3( );
   System.Single logic_uScriptAct_GetComponentsVector3_X_61;
   System.Single logic_uScriptAct_GetComponentsVector3_Y_61;
   System.Single logic_uScriptAct_GetComponentsVector3_Z_61;
   bool logic_uScriptAct_GetComponentsVector3_Out_61 = true;
   //pointer to script instanced logic node
   uScriptAct_SetComponentsVector3 logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_63 = new uScriptAct_SetComponentsVector3( );
   System.Single logic_uScriptAct_SetComponentsVector3_X_63 = (float) 0;
   System.Single logic_uScriptAct_SetComponentsVector3_Y_63 = (float) 0;
   System.Single logic_uScriptAct_SetComponentsVector3_Z_63 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_SetComponentsVector3_OutputVector3_63;
   bool logic_uScriptAct_SetComponentsVector3_Out_63 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectPosition logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_65 = new uScriptAct_SetGameObjectPosition( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectPosition_Target_65 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectPosition_Position_65 = new Vector3( );
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsOffset_65 = (bool) false;
   System.Boolean logic_uScriptAct_SetGameObjectPosition_AsLocal_65 = (bool) false;
   bool logic_uScriptAct_SetGameObjectPosition_Out_65 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_1 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_GameObject_18 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_24 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_28 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_y_28 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_28 = (float) 0;
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_Player_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_Player_UnityEngine_GameObject = GameObject.Find( "/Player" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Player_UnityEngine_GameObject_previous != local_Player_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Player_UnityEngine_GameObject_previous = local_Player_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_13_UnityEngine_Transform || false == m_RegisteredForEvents )
      {
         GameObject gameObject = GameObject.Find( "Player" );
         if ( null != gameObject )
         {
            local_13_UnityEngine_Transform = gameObject.GetComponent<UnityEngine.Transform>();
         }
      }
      if ( null == local_34_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_34_UnityEngine_GameObject = GameObject.Find( "GroundCheck" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_34_UnityEngine_GameObject_previous != local_34_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_34_UnityEngine_GameObject_previous )
         {
            {
               uScript_Trigger2D component = local_34_UnityEngine_GameObject_previous.GetComponent<uScript_Trigger2D>();
               if ( null != component )
               {
                  component.OnEnterTrigger -= Instance_OnEnterTrigger_18;
                  component.OnExitTrigger -= Instance_OnExitTrigger_18;
                  component.WhileInsideTrigger -= Instance_WhileInsideTrigger_18;
               }
            }
         }
         
         local_34_UnityEngine_GameObject_previous = local_34_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_34_UnityEngine_GameObject )
         {
            {
               uScript_Trigger2D component = local_34_UnityEngine_GameObject.GetComponent<uScript_Trigger2D>();
               if ( null == component )
               {
                  component = local_34_UnityEngine_GameObject.AddComponent<uScript_Trigger2D>();
               }
               if ( null != component )
               {
                  component.OnEnterTrigger += Instance_OnEnterTrigger_18;
                  component.OnExitTrigger += Instance_OnExitTrigger_18;
                  component.WhileInsideTrigger += Instance_WhileInsideTrigger_18;
               }
            }
         }
      }
      if ( null == local_58_UnityEngine_Transform || false == m_RegisteredForEvents )
      {
         GameObject gameObject = GameObject.Find( "Cam Target" );
         if ( null != gameObject )
         {
            local_58_UnityEngine_Transform = gameObject.GetComponent<UnityEngine.Transform>();
         }
      }
      if ( null == local_67_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_67_UnityEngine_GameObject = GameObject.Find( "/Player/Cam Target" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_67_UnityEngine_GameObject_previous != local_67_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_67_UnityEngine_GameObject_previous = local_67_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_27 || false == m_RegisteredForEvents )
      {
         owner_Connection_27 = parentGameObject;
      }
      if ( null == owner_Connection_32 || false == m_RegisteredForEvents )
      {
         owner_Connection_32 = parentGameObject;
      }
      if ( null == owner_Connection_42 || false == m_RegisteredForEvents )
      {
         owner_Connection_42 = parentGameObject;
      }
      if ( null == owner_Connection_51 || false == m_RegisteredForEvents )
      {
         owner_Connection_51 = parentGameObject;
      }
      if ( null == owner_Connection_53 || false == m_RegisteredForEvents )
      {
         owner_Connection_53 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_Player_UnityEngine_GameObject_previous != local_Player_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_Player_UnityEngine_GameObject_previous = local_Player_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_34_UnityEngine_GameObject_previous != local_34_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         if ( null != local_34_UnityEngine_GameObject_previous )
         {
            {
               uScript_Trigger2D component = local_34_UnityEngine_GameObject_previous.GetComponent<uScript_Trigger2D>();
               if ( null != component )
               {
                  component.OnEnterTrigger -= Instance_OnEnterTrigger_18;
                  component.OnExitTrigger -= Instance_OnExitTrigger_18;
                  component.WhileInsideTrigger -= Instance_WhileInsideTrigger_18;
               }
            }
         }
         
         local_34_UnityEngine_GameObject_previous = local_34_UnityEngine_GameObject;
         
         //setup new listeners
         if ( null != local_34_UnityEngine_GameObject )
         {
            {
               uScript_Trigger2D component = local_34_UnityEngine_GameObject.GetComponent<uScript_Trigger2D>();
               if ( null == component )
               {
                  component = local_34_UnityEngine_GameObject.AddComponent<uScript_Trigger2D>();
               }
               if ( null != component )
               {
                  component.OnEnterTrigger += Instance_OnEnterTrigger_18;
                  component.OnExitTrigger += Instance_OnExitTrigger_18;
                  component.WhileInsideTrigger += Instance_WhileInsideTrigger_18;
               }
            }
         }
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_67_UnityEngine_GameObject_previous != local_67_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_67_UnityEngine_GameObject_previous = local_67_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_1 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_1 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_1 )
         {
            {
               uScript_Input component = event_UnityEngine_GameObject_Instance_1.GetComponent<uScript_Input>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_1.AddComponent<uScript_Input>();
               }
               if ( null != component )
               {
                  component.KeyEvent += Instance_KeyEvent_1;
               }
            }
         }
      }
      if ( null == event_UnityEngine_GameObject_Instance_24 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_24 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_24 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_24.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_24.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_24;
                  component.OnLateUpdate += Instance_OnLateUpdate_24;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_24;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != local_34_UnityEngine_GameObject )
      {
         {
            uScript_Trigger2D component = local_34_UnityEngine_GameObject.GetComponent<uScript_Trigger2D>();
            if ( null != component )
            {
               component.OnEnterTrigger -= Instance_OnEnterTrigger_18;
               component.OnExitTrigger -= Instance_OnExitTrigger_18;
               component.WhileInsideTrigger -= Instance_WhileInsideTrigger_18;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_1 )
      {
         {
            uScript_Input component = event_UnityEngine_GameObject_Instance_1.GetComponent<uScript_Input>();
            if ( null != component )
            {
               component.KeyEvent -= Instance_KeyEvent_1;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_24 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_24.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_24;
               component.OnLateUpdate -= Instance_OnLateUpdate_24;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_24;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_SetBool_uScriptAct_SetBool_2.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_4.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5.SetParent(g);
      logic_uScriptAct_OnMultipleInputEventFilter_uScriptAct_OnMultipleInputEventFilter_9.SetParent(g);
      logic_uScriptAct_OnMultipleInputEventFilter_uScriptAct_OnMultipleInputEventFilter_10.SetParent(g);
      logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_11.SetParent(g);
      logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_12.SetParent(g);
      logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_16.SetParent(g);
      logic_uScriptAct_AddForce2D_uScriptAct_AddForce2D_19.SetParent(g);
      logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_21.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_22.SetParent(g);
      logic_uScriptAct_SetLayer_uScriptAct_SetLayer_31.SetParent(g);
      logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_35.SetParent(g);
      logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_36.SetParent(g);
      logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_38.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_41.SetParent(g);
      logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_44.SetParent(g);
      logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_45.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_52.SetParent(g);
      logic_uScriptAct_SetLayer_uScriptAct_SetLayer_54.SetParent(g);
      logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_55.SetParent(g);
      logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_57.SetParent(g);
      logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_60.SetParent(g);
      logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_61.SetParent(g);
      logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_63.SetParent(g);
      logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_65.SetParent(g);
      owner_Connection_27 = parentGameObject;
      owner_Connection_32 = parentGameObject;
      owner_Connection_42 = parentGameObject;
      owner_Connection_51 = parentGameObject;
      owner_Connection_53 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_KeyEvent_1(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_KeyEvent_1( );
   }
   
   void Instance_OnEnterTrigger_18(object o, uScript_Trigger2D.TriggerEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_GameObject_18 = e.GameObject;
      //relay event to nodes
      Relay_OnEnterTrigger_18( );
   }
   
   void Instance_OnExitTrigger_18(object o, uScript_Trigger2D.TriggerEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_GameObject_18 = e.GameObject;
      //relay event to nodes
      Relay_OnExitTrigger_18( );
   }
   
   void Instance_WhileInsideTrigger_18(object o, uScript_Trigger2D.TriggerEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_GameObject_18 = e.GameObject;
      //relay event to nodes
      Relay_WhileInsideTrigger_18( );
   }
   
   void Instance_OnUpdate_24(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_24( );
   }
   
   void Instance_OnLateUpdate_24(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_24( );
   }
   
   void Instance_OnFixedUpdate_24(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_24( );
   }
   
   void Relay_KeyEvent_1()
   {
      if (true == CheckDebugBreak("60bced46-47aa-4c8b-b4d9-c1572d76647a", "Input_Events", Relay_KeyEvent_1)) return; 
      Relay_In_9();
      Relay_In_10();
   }
   
   void Relay_True_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("631b390b-dfd5-4e8b-a17d-996b1bdfabaa", "Set_Bool", Relay_True_2)) return; 
         {
            {
            }
         }
         logic_uScriptAct_SetBool_uScriptAct_SetBool_2.True(out logic_uScriptAct_SetBool_Target_2);
         Jump_Check = logic_uScriptAct_SetBool_Target_2;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Set Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_False_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("631b390b-dfd5-4e8b-a17d-996b1bdfabaa", "Set_Bool", Relay_False_2)) return; 
         {
            {
            }
         }
         logic_uScriptAct_SetBool_uScriptAct_SetBool_2.False(out logic_uScriptAct_SetBool_Target_2);
         Jump_Check = logic_uScriptAct_SetBool_Target_2;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Set Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_4()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("86205bf8-d5c0-44b7-a72c-0fdbe2dfc11a", "Compare_Bool", Relay_In_4)) return; 
         {
            {
               logic_uScriptCon_CompareBool_Bool_4 = Jump_Check;
               
            }
         }
         logic_uScriptCon_CompareBool_uScriptCon_CompareBool_4.In(logic_uScriptCon_CompareBool_Bool_4);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_4.True;
         
         if ( test_0 == true )
         {
            Relay_In_5();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Compare Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_5()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("7591fa7a-d825-4da1-9cc9-fb9f5238c6d8", "Multiply_Float", Relay_In_5)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_5 = local_7_System_Single;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_5 = Jump_Force;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5.In(logic_uScriptAct_MultiplyFloat_v2_A_5, logic_uScriptAct_MultiplyFloat_v2_B_5, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_5, out logic_uScriptAct_MultiplyFloat_v2_IntResult_5);
         local_6_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_5;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5.Out;
         
         if ( test_0 == true )
         {
            Relay_In_21();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("11083dc8-9791-4886-aa72-ab4de56bf895", "Multiple_Input_Events_Filter", Relay_In_9)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnMultipleInputEventFilter_uScriptAct_OnMultipleInputEventFilter_9.In(logic_uScriptAct_OnMultipleInputEventFilter_KeyCode_9);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Multiple Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_10()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("bdb6b8ff-22e9-4771-a127-06186fbe5757", "Multiple_Input_Events_Filter", Relay_In_10)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnMultipleInputEventFilter_uScriptAct_OnMultipleInputEventFilter_10.In(logic_uScriptAct_OnMultipleInputEventFilter_KeyCode_10);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnMultipleInputEventFilter_uScriptAct_OnMultipleInputEventFilter_10.KeyDown;
         
         if ( test_0 == true )
         {
            Relay_In_12();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Multiple Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_11()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("bd91c482-c9c5-40df-82f8-f341897e3886", "Get_Components__Vector3_", Relay_In_11)) return; 
         {
            {
               logic_uScriptAct_GetComponentsVector3_InputVector3_11 = local_14_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_11.In(logic_uScriptAct_GetComponentsVector3_InputVector3_11, out logic_uScriptAct_GetComponentsVector3_X_11, out logic_uScriptAct_GetComponentsVector3_Y_11, out logic_uScriptAct_GetComponentsVector3_Z_11);
         local_15_System_Single = logic_uScriptAct_GetComponentsVector3_Z_11;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_11.Out;
         
         if ( test_0 == true )
         {
            Relay_In_16();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Get Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_12()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("550fc584-a61d-4c22-8ffb-279fd633a69f", "Get_Position_From_Transform", Relay_In_12)) return; 
         {
            {
               logic_uScriptAct_GetPositionFromTransform_target_12 = local_13_UnityEngine_Transform;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_12.In(logic_uScriptAct_GetPositionFromTransform_target_12, logic_uScriptAct_GetPositionFromTransform_getLocal_12, out logic_uScriptAct_GetPositionFromTransform_position_12, out logic_uScriptAct_GetPositionFromTransform_forward_12, out logic_uScriptAct_GetPositionFromTransform_right_12, out logic_uScriptAct_GetPositionFromTransform_up_12, out logic_uScriptAct_GetPositionFromTransform_worldMatrix_12);
         local_14_UnityEngine_Vector3 = logic_uScriptAct_GetPositionFromTransform_position_12;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_12.Out;
         
         if ( test_0 == true )
         {
            Relay_In_11();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Get Position From Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_16()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("096019b4-524e-49e2-a359-8a13fa572265", "Compare_Float", Relay_In_16)) return; 
         {
            {
               logic_uScriptCon_CompareFloat_A_16 = local_15_System_Single;
               
            }
            {
               logic_uScriptCon_CompareFloat_B_16 = local_17_System_Single;
               
            }
         }
         logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_16.In(logic_uScriptCon_CompareFloat_A_16, logic_uScriptCon_CompareFloat_B_16);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_16.GreaterThan;
         bool test_1 = logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_16.LessThan;
         
         if ( test_0 == true )
         {
            Relay_In_54();
         }
         if ( test_1 == true )
         {
            Relay_In_31();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Compare Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnEnterTrigger_18()
   {
      if (true == CheckDebugBreak("31ec9b91-4995-4154-96c9-ef566cb5f5d6", "Trigger_Event__2D_", Relay_OnEnterTrigger_18)) return; 
      Relay_True_2();
   }
   
   void Relay_OnExitTrigger_18()
   {
      if (true == CheckDebugBreak("31ec9b91-4995-4154-96c9-ef566cb5f5d6", "Trigger_Event__2D_", Relay_OnExitTrigger_18)) return; 
      Relay_False_2();
   }
   
   void Relay_WhileInsideTrigger_18()
   {
      if (true == CheckDebugBreak("31ec9b91-4995-4154-96c9-ef566cb5f5d6", "Trigger_Event__2D_", Relay_WhileInsideTrigger_18)) return; 
      Relay_True_2();
   }
   
   void Relay_In_19()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("fff1761f-ba1b-442b-b6da-459c816fb5c4", "Add_Force__2D_", Relay_In_19)) return; 
         {
            {
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_Player_UnityEngine_GameObject_previous != local_Player_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_Player_UnityEngine_GameObject_previous = local_Player_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               logic_uScriptAct_AddForce2D_Target_19 = local_Player_UnityEngine_GameObject;
               
            }
            {
               logic_uScriptAct_AddForce2D_force_19 = local_20_UnityEngine_Vector2;
               
            }
            {
            }
         }
         logic_uScriptAct_AddForce2D_uScriptAct_AddForce2D_19.In(logic_uScriptAct_AddForce2D_Target_19, logic_uScriptAct_AddForce2D_force_19, logic_uScriptAct_AddForce2D_scale_19);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Add Force (2D).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_21()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("bc7b0811-c38c-491f-a034-5a9e6c005359", "Replace_Components__Vector2_", Relay_In_21)) return; 
         {
            {
               logic_uScriptAct_ReplaceComponentsVector2_Target_21 = local_20_UnityEngine_Vector2;
               
            }
            {
            }
            {
            }
            {
               logic_uScriptAct_ReplaceComponentsVector2_Y_21 = local_6_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_21.In(logic_uScriptAct_ReplaceComponentsVector2_Target_21, logic_uScriptAct_ReplaceComponentsVector2_X_21, logic_uScriptAct_ReplaceComponentsVector2_IgnoreX_21, logic_uScriptAct_ReplaceComponentsVector2_Y_21, logic_uScriptAct_ReplaceComponentsVector2_IgnoreY_21, out logic_uScriptAct_ReplaceComponentsVector2_Output_21);
         local_20_UnityEngine_Vector2 = logic_uScriptAct_ReplaceComponentsVector2_Output_21;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_ReplaceComponentsVector2_uScriptAct_ReplaceComponentsVector2_21.Out;
         
         if ( test_0 == true )
         {
            Relay_In_19();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Replace Components (Vector2).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_22()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("8b5da52f-261e-43a3-bb30-f74d78a9d1de", "Multiply_Float", Relay_In_22)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_22 = local_25_System_Single;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_22 = Player_Move_Speed;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_22.In(logic_uScriptAct_MultiplyFloat_v2_A_22, logic_uScriptAct_MultiplyFloat_v2_B_22, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_22, out logic_uScriptAct_MultiplyFloat_v2_IntResult_22);
         local_26_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_22;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_22.Out;
         
         if ( test_0 == true )
         {
            Relay_Translate_28();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnUpdate_24()
   {
      if (true == CheckDebugBreak("c53d1dd9-41cb-46ca-98ad-9faef70189c4", "Global_Update", Relay_OnUpdate_24)) return; 
      Relay_In_22();
   }
   
   void Relay_OnLateUpdate_24()
   {
      if (true == CheckDebugBreak("c53d1dd9-41cb-46ca-98ad-9faef70189c4", "Global_Update", Relay_OnLateUpdate_24)) return; 
   }
   
   void Relay_OnFixedUpdate_24()
   {
      if (true == CheckDebugBreak("c53d1dd9-41cb-46ca-98ad-9faef70189c4", "Global_Update", Relay_OnFixedUpdate_24)) return; 
   }
   
   void Relay_Translate_28()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("79ef144d-0122-46b6-98cc-023dbeb78778", "UnityEngine_Transform", Relay_Translate_28)) return; 
         {
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_28 = local_26_System_Single;
               
            }
            {
            }
            {
            }
         }
         {
            UnityEngine.Transform component;
            component = owner_Connection_27.GetComponent<UnityEngine.Transform>();
            if ( null != component )
            {
               component.Translate(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_28, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_y_28, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_28);
            }
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at UnityEngine.Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_31()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("89951ab3-2f1a-4c93-ad70-000b11f37979", "Set_Layer", Relay_In_31)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_SetLayer_Target_31.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetLayer_Target_31, index + 1);
               }
               logic_uScriptAct_SetLayer_Target_31[ index++ ] = owner_Connection_32;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_SetLayer_uScriptAct_SetLayer_31.In(logic_uScriptAct_SetLayer_Target_31, logic_uScriptAct_SetLayer_Layer_31, logic_uScriptAct_SetLayer_ApplyToChildren_31);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_SetLayer_uScriptAct_SetLayer_31.Out;
         
         if ( test_0 == true )
         {
            Relay_In_36();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Set Layer.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_35()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("294160af-acfb-482e-9859-816e2c6ca5e1", "Set_Components__Vector3_", Relay_In_35)) return; 
         {
            {
               logic_uScriptAct_SetComponentsVector3_X_35 = local_39_System_Single;
               
            }
            {
               logic_uScriptAct_SetComponentsVector3_Y_35 = local_40_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_35.In(logic_uScriptAct_SetComponentsVector3_X_35, logic_uScriptAct_SetComponentsVector3_Y_35, logic_uScriptAct_SetComponentsVector3_Z_35, out logic_uScriptAct_SetComponentsVector3_OutputVector3_35);
         local_43_UnityEngine_Vector3 = logic_uScriptAct_SetComponentsVector3_OutputVector3_35;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_35.Out;
         
         if ( test_0 == true )
         {
            Relay_In_41();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Set Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_36()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("46de5c5f-f620-4af1-a652-579be6dbcff8", "Get_Position_and_Rotation", Relay_In_36)) return; 
         {
            {
               logic_uScriptAct_GetPositionAndRotation_Target_36 = owner_Connection_32;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_36.In(logic_uScriptAct_GetPositionAndRotation_Target_36, logic_uScriptAct_GetPositionAndRotation_GetLocal_36, out logic_uScriptAct_GetPositionAndRotation_Position_36, out logic_uScriptAct_GetPositionAndRotation_Rotation_36, out logic_uScriptAct_GetPositionAndRotation_EulerAngles_36, out logic_uScriptAct_GetPositionAndRotation_Forward_36, out logic_uScriptAct_GetPositionAndRotation_Up_36, out logic_uScriptAct_GetPositionAndRotation_Right_36);
         local_37_UnityEngine_Vector3 = logic_uScriptAct_GetPositionAndRotation_Position_36;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_36.Out;
         
         if ( test_0 == true )
         {
            Relay_In_38();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Get Position and Rotation.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_38()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("cd1ba5bf-4637-49df-8eb0-cb6597820219", "Get_Components__Vector3_", Relay_In_38)) return; 
         {
            {
               logic_uScriptAct_GetComponentsVector3_InputVector3_38 = local_37_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_38.In(logic_uScriptAct_GetComponentsVector3_InputVector3_38, out logic_uScriptAct_GetComponentsVector3_X_38, out logic_uScriptAct_GetComponentsVector3_Y_38, out logic_uScriptAct_GetComponentsVector3_Z_38);
         local_39_System_Single = logic_uScriptAct_GetComponentsVector3_X_38;
         local_40_System_Single = logic_uScriptAct_GetComponentsVector3_Y_38;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_38.Out;
         
         if ( test_0 == true )
         {
            Relay_In_35();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Get Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_41()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("91df8c4d-5d42-4f66-ae6c-1cc85103befc", "Set_Position", Relay_In_41)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_SetGameObjectPosition_Target_41.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_41, index + 1);
               }
               logic_uScriptAct_SetGameObjectPosition_Target_41[ index++ ] = owner_Connection_42;
               
            }
            {
               logic_uScriptAct_SetGameObjectPosition_Position_41 = local_43_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_41.In(logic_uScriptAct_SetGameObjectPosition_Target_41, logic_uScriptAct_SetGameObjectPosition_Position_41, logic_uScriptAct_SetGameObjectPosition_AsOffset_41, logic_uScriptAct_SetGameObjectPosition_AsLocal_41);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Set Position.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_44()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("ffa30b99-cc94-48af-b00d-d782e7ecafc1", "Get_Components__Vector3_", Relay_In_44)) return; 
         {
            {
               logic_uScriptAct_GetComponentsVector3_InputVector3_44 = local_47_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_44.In(logic_uScriptAct_GetComponentsVector3_InputVector3_44, out logic_uScriptAct_GetComponentsVector3_X_44, out logic_uScriptAct_GetComponentsVector3_Y_44, out logic_uScriptAct_GetComponentsVector3_Z_44);
         local_46_System_Single = logic_uScriptAct_GetComponentsVector3_X_44;
         local_50_System_Single = logic_uScriptAct_GetComponentsVector3_Y_44;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_44.Out;
         
         if ( test_0 == true )
         {
            Relay_In_55();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Get Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_45()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("54d9c068-e3b5-4560-b5ad-817f138a1f9e", "Get_Position_and_Rotation", Relay_In_45)) return; 
         {
            {
               logic_uScriptAct_GetPositionAndRotation_Target_45 = owner_Connection_51;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_45.In(logic_uScriptAct_GetPositionAndRotation_Target_45, logic_uScriptAct_GetPositionAndRotation_GetLocal_45, out logic_uScriptAct_GetPositionAndRotation_Position_45, out logic_uScriptAct_GetPositionAndRotation_Rotation_45, out logic_uScriptAct_GetPositionAndRotation_EulerAngles_45, out logic_uScriptAct_GetPositionAndRotation_Forward_45, out logic_uScriptAct_GetPositionAndRotation_Up_45, out logic_uScriptAct_GetPositionAndRotation_Right_45);
         local_47_UnityEngine_Vector3 = logic_uScriptAct_GetPositionAndRotation_Position_45;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetPositionAndRotation_uScriptAct_GetPositionAndRotation_45.Out;
         
         if ( test_0 == true )
         {
            Relay_In_44();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Get Position and Rotation.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_52()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("e7b1114a-c839-4f8a-8e79-aa51aecc2533", "Set_Position", Relay_In_52)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_SetGameObjectPosition_Target_52.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_52, index + 1);
               }
               logic_uScriptAct_SetGameObjectPosition_Target_52[ index++ ] = owner_Connection_53;
               
            }
            {
               logic_uScriptAct_SetGameObjectPosition_Position_52 = local_49_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_52.In(logic_uScriptAct_SetGameObjectPosition_Target_52, logic_uScriptAct_SetGameObjectPosition_Position_52, logic_uScriptAct_SetGameObjectPosition_AsOffset_52, logic_uScriptAct_SetGameObjectPosition_AsLocal_52);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Set Position.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_54()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("ff1431e9-2f3e-424c-8cdb-6e2400e9f21b", "Set_Layer", Relay_In_54)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_SetLayer_Target_54.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetLayer_Target_54, index + 1);
               }
               logic_uScriptAct_SetLayer_Target_54[ index++ ] = owner_Connection_51;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_SetLayer_uScriptAct_SetLayer_54.In(logic_uScriptAct_SetLayer_Target_54, logic_uScriptAct_SetLayer_Layer_54, logic_uScriptAct_SetLayer_ApplyToChildren_54);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_SetLayer_uScriptAct_SetLayer_54.Out;
         
         if ( test_0 == true )
         {
            Relay_In_45();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Set Layer.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_55()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d7902b7a-b7db-4f28-9874-c770dd0c4563", "Set_Components__Vector3_", Relay_In_55)) return; 
         {
            {
               logic_uScriptAct_SetComponentsVector3_X_55 = local_46_System_Single;
               
            }
            {
               logic_uScriptAct_SetComponentsVector3_Y_55 = local_50_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_55.In(logic_uScriptAct_SetComponentsVector3_X_55, logic_uScriptAct_SetComponentsVector3_Y_55, logic_uScriptAct_SetComponentsVector3_Z_55, out logic_uScriptAct_SetComponentsVector3_OutputVector3_55);
         local_49_UnityEngine_Vector3 = logic_uScriptAct_SetComponentsVector3_OutputVector3_55;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_55.Out;
         
         if ( test_0 == true )
         {
            Relay_In_52();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Set Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_57()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("5fe8bd60-10ec-43e6-bad5-f7340079272b", "Get_Position_From_Transform", Relay_In_57)) return; 
         {
            {
               logic_uScriptAct_GetPositionFromTransform_target_57 = local_58_UnityEngine_Transform;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_57.In(logic_uScriptAct_GetPositionFromTransform_target_57, logic_uScriptAct_GetPositionFromTransform_getLocal_57, out logic_uScriptAct_GetPositionFromTransform_position_57, out logic_uScriptAct_GetPositionFromTransform_forward_57, out logic_uScriptAct_GetPositionFromTransform_right_57, out logic_uScriptAct_GetPositionFromTransform_up_57, out logic_uScriptAct_GetPositionFromTransform_worldMatrix_57);
         local_59_UnityEngine_Vector3 = logic_uScriptAct_GetPositionFromTransform_position_57;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_57.Out;
         
         if ( test_0 == true )
         {
            Relay_In_61();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Get Position From Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_60()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("69819122-d9ec-4c03-aa20-cad4604438b0", "Compare_Float", Relay_In_60)) return; 
         {
            {
               logic_uScriptCon_CompareFloat_A_60 = Player_Move_Speed;
               
            }
            {
               logic_uScriptCon_CompareFloat_B_60 = local_62_System_Single;
               
            }
         }
         logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_60.In(logic_uScriptCon_CompareFloat_A_60, logic_uScriptCon_CompareFloat_B_60);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptCon_CompareFloat_uScriptCon_CompareFloat_60.NotEqualTo;
         
         if ( test_0 == true )
         {
            Relay_In_63();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Compare Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_61()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("6049fce7-be2f-4e42-97e6-1b2ed444dc38", "Get_Components__Vector3_", Relay_In_61)) return; 
         {
            {
               logic_uScriptAct_GetComponentsVector3_InputVector3_61 = local_59_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_61.In(logic_uScriptAct_GetComponentsVector3_InputVector3_61, out logic_uScriptAct_GetComponentsVector3_X_61, out logic_uScriptAct_GetComponentsVector3_Y_61, out logic_uScriptAct_GetComponentsVector3_Z_61);
         local_62_System_Single = logic_uScriptAct_GetComponentsVector3_X_61;
         local_68_System_Single = logic_uScriptAct_GetComponentsVector3_Y_61;
         local_69_System_Single = logic_uScriptAct_GetComponentsVector3_Z_61;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Get Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_63()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("e34f4a21-17da-470d-846d-f4d1d2adc03f", "Set_Components__Vector3_", Relay_In_63)) return; 
         {
            {
               logic_uScriptAct_SetComponentsVector3_X_63 = Player_Move_Speed;
               
            }
            {
               logic_uScriptAct_SetComponentsVector3_Y_63 = local_68_System_Single;
               
            }
            {
               logic_uScriptAct_SetComponentsVector3_Z_63 = local_69_System_Single;
               
            }
            {
            }
         }
         logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_63.In(logic_uScriptAct_SetComponentsVector3_X_63, logic_uScriptAct_SetComponentsVector3_Y_63, logic_uScriptAct_SetComponentsVector3_Z_63, out logic_uScriptAct_SetComponentsVector3_OutputVector3_63);
         local_64_UnityEngine_Vector3 = logic_uScriptAct_SetComponentsVector3_OutputVector3_63;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_63.Out;
         
         if ( test_0 == true )
         {
            Relay_In_65();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Set Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_65()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("786ebcf8-d31e-4108-90a9-08c84fcef23d", "Set_Position", Relay_In_65)) return; 
         {
            {
               int index = 0;
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_67_UnityEngine_GameObject_previous != local_67_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_67_UnityEngine_GameObject_previous = local_67_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               if ( logic_uScriptAct_SetGameObjectPosition_Target_65.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetGameObjectPosition_Target_65, index + 1);
               }
               logic_uScriptAct_SetGameObjectPosition_Target_65[ index++ ] = local_67_UnityEngine_GameObject;
               
            }
            {
               logic_uScriptAct_SetGameObjectPosition_Position_65 = local_64_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_SetGameObjectPosition_uScriptAct_SetGameObjectPosition_65.In(logic_uScriptAct_SetGameObjectPosition_Target_65, logic_uScriptAct_SetGameObjectPosition_Position_65, logic_uScriptAct_SetGameObjectPosition_AsOffset_65, logic_uScriptAct_SetGameObjectPosition_AsLocal_65);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript PlayerController.uscript at Set Position.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:Player", local_Player_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "92761f73-b8d0-4434-96ea-3edcc1948567", local_Player_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:Jump Check", Jump_Check);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "58b0da56-73f1-4d8f-a815-eda107e899b1", Jump_Check);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:6", local_6_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "3b2fd26f-16a8-45e8-96a6-2957fa25f9ef", local_6_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:7", local_7_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "7ce8ddf1-0740-47ac-9cbd-c314cd030cb5", local_7_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:Jump Force", Jump_Force);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "7ca75d8b-b109-4071-bd09-aa17c29f01c5", Jump_Force);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:13", local_13_UnityEngine_Transform);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "8a25de5c-3acd-4b11-b0d7-4b4362eed2a8", local_13_UnityEngine_Transform);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:14", local_14_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "8c44a7c4-bb42-43f4-8b56-eb9aa63e7d97", local_14_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:15", local_15_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "7b3ecd80-6063-4376-a3e3-edd3dfdfbef1", local_15_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:17", local_17_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "0a2d2516-480e-46c8-b1b0-37b9668a12b3", local_17_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:20", local_20_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "d8d6c53e-f7f3-46a3-9b6d-52a651cdfb48", local_20_UnityEngine_Vector2);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:Player Move Speed", Player_Move_Speed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "11ca2b7e-ad96-49ae-a021-2447f7adf954", Player_Move_Speed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:25", local_25_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "f3f64dc1-f065-4ff6-bc6d-3cd16b9ee3f0", local_25_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:26", local_26_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "2d4b9e36-0a15-4a5d-8733-89eea3cbe3a4", local_26_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:34", local_34_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "64c7b958-1c52-4d0b-8379-9499b573805a", local_34_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:37", local_37_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "69bf1e4a-ab73-47be-8263-910f79e04907", local_37_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:39", local_39_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "01054587-376d-4463-a73e-c8e039b62a64", local_39_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:40", local_40_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "53f2cce8-57bc-4041-ac59-037e49c2989a", local_40_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:43", local_43_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "a0017e63-d1f7-40f4-b6de-f0ffe7c413d5", local_43_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:46", local_46_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "fd0c3ecf-6e29-495d-b1dc-2949b987ef04", local_46_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:47", local_47_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "a5207130-9b56-4d75-9514-e33bf5a6bc0f", local_47_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:49", local_49_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "9f2393ff-9ad1-43ec-ab55-3230d8185b2c", local_49_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:50", local_50_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "c39de5ae-652d-45c1-aeee-3c27b2218280", local_50_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:58", local_58_UnityEngine_Transform);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "d6b1843e-c0d9-4714-95f1-d6129323e039", local_58_UnityEngine_Transform);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:59", local_59_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "4bc3f381-5693-4175-aab7-352d7aeb64e9", local_59_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:62", local_62_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "514ce12a-f751-4cc1-b944-e5788f65e9ca", local_62_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:64", local_64_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "956c7efe-5f1d-41b0-a192-92d8cd01d1ee", local_64_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:67", local_67_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "f27dda1a-d858-4cdc-8af7-e369d05cfcff", local_67_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:68", local_68_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "76a65f26-c01b-4e22-b1ec-32c07f4b9004", local_68_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "PlayerController.uscript:69", local_69_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "603faa4f-084a-425d-bd60-53432c26c4c0", local_69_System_Single);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
