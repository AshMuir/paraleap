//uScript Generated Code - Build 1.0.3072
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class GameController : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   [HideInInspector]
   public System.Boolean Player_Lost_ = (bool) false;
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_1 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_1 = "AshMScene";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_1 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_1 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_1 = true;
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_2 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_2 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_2 = true;
   bool logic_uScriptCon_CompareBool_False_2 = true;
   //pointer to script instanced logic node
   uScript_GraphEvents logic_uScript_GraphEvents_uScript_GraphEvents_7 = new uScript_GraphEvents( );
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_8 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_8;
   bool logic_uScriptAct_SetBool_Out_8 = true;
   bool logic_uScriptAct_SetBool_SetTrue_8 = true;
   bool logic_uScriptAct_SetBool_SetFalse_8 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_0;
                  component.OnLateUpdate += Instance_OnLateUpdate_0;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_0;
               component.OnLateUpdate -= Instance_OnLateUpdate_0;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_1.SetParent(g);
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_2.SetParent(g);
      logic_uScript_GraphEvents_uScript_GraphEvents_7.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_8.SetParent(g);
   }
   public void Awake()
   {
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_1.Loaded += uScriptAct_LoadLevel_Loaded_1;
      logic_uScript_GraphEvents_uScript_GraphEvents_7.uScriptEnable += uScript_GraphEvents_uScriptEnable_7;
      logic_uScript_GraphEvents_uScript_GraphEvents_7.uScriptDisable += uScript_GraphEvents_uScriptDisable_7;
      logic_uScript_GraphEvents_uScript_GraphEvents_7.uScriptDestroy += uScript_GraphEvents_uScriptDestroy_7;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
      logic_uScript_GraphEvents_uScript_GraphEvents_7.OnEnable( );
   }
   
   public void OnDisable()
   {
      logic_uScript_GraphEvents_uScript_GraphEvents_7.OnDisable( );
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_1.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScript_GraphEvents_uScript_GraphEvents_7.OnDestroy( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_1.Loaded -= uScriptAct_LoadLevel_Loaded_1;
      logic_uScript_GraphEvents_uScript_GraphEvents_7.uScriptEnable -= uScript_GraphEvents_uScriptEnable_7;
      logic_uScript_GraphEvents_uScript_GraphEvents_7.uScriptDisable -= uScript_GraphEvents_uScriptDisable_7;
      logic_uScript_GraphEvents_uScript_GraphEvents_7.uScriptDestroy -= uScript_GraphEvents_uScriptDestroy_7;
   }
   
   void Instance_OnUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_0( );
   }
   
   void Instance_OnLateUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_0( );
   }
   
   void Instance_OnFixedUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_0( );
   }
   
   void uScriptAct_LoadLevel_Loaded_1(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_1( );
   }
   
   void uScript_GraphEvents_uScriptEnable_7(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptEnable_7( );
   }
   
   void uScript_GraphEvents_uScriptDisable_7(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptDisable_7( );
   }
   
   void uScript_GraphEvents_uScriptDestroy_7(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptDestroy_7( );
   }
   
   void Relay_OnUpdate_0()
   {
      if (true == CheckDebugBreak("148823bd-3ea4-4eec-a7e4-83c9094094fa", "Global_Update", Relay_OnUpdate_0)) return; 
      Relay_In_2();
   }
   
   void Relay_OnLateUpdate_0()
   {
      if (true == CheckDebugBreak("148823bd-3ea4-4eec-a7e4-83c9094094fa", "Global_Update", Relay_OnLateUpdate_0)) return; 
   }
   
   void Relay_OnFixedUpdate_0()
   {
      if (true == CheckDebugBreak("148823bd-3ea4-4eec-a7e4-83c9094094fa", "Global_Update", Relay_OnFixedUpdate_0)) return; 
   }
   
   void Relay_Loaded_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d4b15337-fd14-4c18-b980-e52f3805fcd6", "Load_Level", Relay_Loaded_1)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript GameController.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d4b15337-fd14-4c18-b980-e52f3805fcd6", "Load_Level", Relay_In_1)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_1.In(logic_uScriptAct_LoadLevel_name_1, logic_uScriptAct_LoadLevel_destroyOtherObjects_1, logic_uScriptAct_LoadLevel_blockUntilLoaded_1);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript GameController.uscript at Load Level.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("35e9f38f-6c45-4889-bffe-8d70829a5a21", "Compare_Bool", Relay_In_2)) return; 
         {
            {
               logic_uScriptCon_CompareBool_Bool_2 = Player_Lost_;
               
            }
         }
         logic_uScriptCon_CompareBool_uScriptCon_CompareBool_2.In(logic_uScriptCon_CompareBool_Bool_2);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_2.True;
         
         if ( test_0 == true )
         {
            Relay_In_1();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript GameController.uscript at Compare Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_uScriptEnable_7()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("38188bd5-9913-48db-9a66-8078b4a449fd", "Graph_Events", Relay_uScriptEnable_7)) return; 
         Relay_False_8();
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript GameController.uscript at Graph Events.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_uScriptDisable_7()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("38188bd5-9913-48db-9a66-8078b4a449fd", "Graph_Events", Relay_uScriptDisable_7)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript GameController.uscript at Graph Events.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_uScriptDestroy_7()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("38188bd5-9913-48db-9a66-8078b4a449fd", "Graph_Events", Relay_uScriptDestroy_7)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript GameController.uscript at Graph Events.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_True_8()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("ac188236-8666-4fbf-b272-f4eb683dbc89", "Set_Bool", Relay_True_8)) return; 
         {
            {
            }
         }
         logic_uScriptAct_SetBool_uScriptAct_SetBool_8.True(out logic_uScriptAct_SetBool_Target_8);
         Player_Lost_ = logic_uScriptAct_SetBool_Target_8;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript GameController.uscript at Set Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_False_8()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("ac188236-8666-4fbf-b272-f4eb683dbc89", "Set_Bool", Relay_False_8)) return; 
         {
            {
            }
         }
         logic_uScriptAct_SetBool_uScriptAct_SetBool_8.False(out logic_uScriptAct_SetBool_Target_8);
         Player_Lost_ = logic_uScriptAct_SetBool_Target_8;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript GameController.uscript at Set Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "GameController.uscript:Player Lost?", Player_Lost_);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "8abee491-5315-4367-a8a1-ca67b6cd68e5", Player_Lost_);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
