//uScript Generated Code - Build 1.0.3072
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class Hazard : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.GameObject local_0_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_0_UnityEngine_GameObject_previous = null;
   System.Boolean local_Is_Player__System_Boolean = (bool) false;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_8 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptCon_CompareBool logic_uScriptCon_CompareBool_uScriptCon_CompareBool_3 = new uScriptCon_CompareBool( );
   System.Boolean logic_uScriptCon_CompareBool_Bool_3 = (bool) false;
   bool logic_uScriptCon_CompareBool_True_3 = true;
   bool logic_uScriptCon_CompareBool_False_3 = true;
   //pointer to script instanced logic node
   uScriptAct_SetBool logic_uScriptAct_SetBool_uScriptAct_SetBool_4 = new uScriptAct_SetBool( );
   System.Boolean logic_uScriptAct_SetBool_Target_4;
   bool logic_uScriptAct_SetBool_Out_4 = true;
   bool logic_uScriptAct_SetBool_SetTrue_4 = true;
   bool logic_uScriptAct_SetBool_SetFalse_4 = true;
   //pointer to script instanced logic node
   uScriptAct_Log logic_uScriptAct_Log_uScriptAct_Log_6 = new uScriptAct_Log( );
   System.Object logic_uScriptAct_Log_Prefix_6 = "";
   System.Object[] logic_uScriptAct_Log_Target_6 = new System.Object[] {};
   System.Object logic_uScriptAct_Log_Postfix_6 = "";
   bool logic_uScriptAct_Log_Out_6 = true;
   //pointer to script instanced logic node
   uScriptAct_Log logic_uScriptAct_Log_uScriptAct_Log_9 = new uScriptAct_Log( );
   System.Object logic_uScriptAct_Log_Prefix_9 = "";
   System.Object[] logic_uScriptAct_Log_Target_9 = new System.Object[] {};
   System.Object logic_uScriptAct_Log_Postfix_9 = "";
   bool logic_uScriptAct_Log_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_Log logic_uScriptAct_Log_uScriptAct_Log_10 = new uScriptAct_Log( );
   System.Object logic_uScriptAct_Log_Prefix_10 = "";
   System.Object[] logic_uScriptAct_Log_Target_10 = new System.Object[] {};
   System.Object logic_uScriptAct_Log_Postfix_10 = "";
   bool logic_uScriptAct_Log_Out_10 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_GameObject_7 = default(UnityEngine.GameObject);
   
   //property nodes
   System.Boolean property_Player_Lost__Detox_ScriptEditor_Parameter_Player_Lost__5 = (bool) false;
   UnityEngine.GameObject property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5 = default(UnityEngine.GameObject);
   UnityEngine.GameObject property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5_previous = null;
   
   //method nodes
   System.String method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_tag_1 = "Player";
   System.Boolean method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_1 = (bool) false;
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   System.Boolean property_Player_Lost__Detox_ScriptEditor_Parameter_Player_Lost__5_Get_Refresh( )
   {
      GameController_Component component = null;
      if (property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5 != null)
      {
         component = property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5.GetComponent<GameController_Component>();
      }
      if ( null != component )
      {
         return component.Player_Lost_;
      }
      else
      {
         return (bool) false;
      }
   }
   
   void property_Player_Lost__Detox_ScriptEditor_Parameter_Player_Lost__5_Set_Refresh( )
   {
      GameController_Component component = null;
      if (property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5 != null)
      {
         component = property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5.GetComponent<GameController_Component>();
      }
      if ( null != component )
      {
         component.Player_Lost_ = property_Player_Lost__Detox_ScriptEditor_Parameter_Player_Lost__5;
      }
   }
   
   
   void SyncUnityHooks( )
   {
      if ( null == property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5 || false == m_RegisteredForEvents )
      {
         property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5 = GameObject.Find( "/_uScript" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5_previous != property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5 || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5_previous = property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5;
         
         //setup new listeners
      }
      SyncEventListeners( );
      if ( null == local_0_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_0_UnityEngine_GameObject = GameObject.Find( "/Player" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_0_UnityEngine_GameObject_previous != local_0_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_0_UnityEngine_GameObject_previous = local_0_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_8 || false == m_RegisteredForEvents )
      {
         owner_Connection_8 = parentGameObject;
         if ( null != owner_Connection_8 )
         {
            {
               uScript_Trigger2D_NoStay component = owner_Connection_8.GetComponent<uScript_Trigger2D_NoStay>();
               if ( null == component )
               {
                  component = owner_Connection_8.AddComponent<uScript_Trigger2D_NoStay>();
               }
               if ( null != component )
               {
                  component.OnEnterTrigger += Instance_OnEnterTrigger_7;
                  component.OnExitTrigger += Instance_OnExitTrigger_7;
               }
            }
         }
      }
   }
   
   void RegisterForUnityHooks( )
   {
      //if our game object reference was changed then we need to reset event listeners
      if ( property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5_previous != property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5 || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5_previous = property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5;
         
         //setup new listeners
      }
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_0_UnityEngine_GameObject_previous != local_0_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_0_UnityEngine_GameObject_previous = local_0_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //reset event listeners if needed
      //this isn't a variable node so it should only be called once per enabling of the script
      //if it's called twice there would be a double event registration (which is an error)
      if ( false == m_RegisteredForEvents )
      {
         if ( null != owner_Connection_8 )
         {
            {
               uScript_Trigger2D_NoStay component = owner_Connection_8.GetComponent<uScript_Trigger2D_NoStay>();
               if ( null == component )
               {
                  component = owner_Connection_8.AddComponent<uScript_Trigger2D_NoStay>();
               }
               if ( null != component )
               {
                  component.OnEnterTrigger += Instance_OnEnterTrigger_7;
                  component.OnExitTrigger += Instance_OnExitTrigger_7;
               }
            }
         }
      }
   }
   
   void SyncEventListeners( )
   {
      {
         {
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5_previous != property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5 || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5_previous = property_Player_Lost__Detox_ScriptEditor_Parameter_Instance_5;
                  
                  //setup new listeners
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != owner_Connection_8 )
      {
         {
            uScript_Trigger2D_NoStay component = owner_Connection_8.GetComponent<uScript_Trigger2D_NoStay>();
            if ( null != component )
            {
               component.OnEnterTrigger -= Instance_OnEnterTrigger_7;
               component.OnExitTrigger -= Instance_OnExitTrigger_7;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptCon_CompareBool_uScriptCon_CompareBool_3.SetParent(g);
      logic_uScriptAct_SetBool_uScriptAct_SetBool_4.SetParent(g);
      logic_uScriptAct_Log_uScriptAct_Log_6.SetParent(g);
      logic_uScriptAct_Log_uScriptAct_Log_9.SetParent(g);
      logic_uScriptAct_Log_uScriptAct_Log_10.SetParent(g);
      owner_Connection_8 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnEnterTrigger_7(object o, uScript_Trigger2D_NoStay.TriggerEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_GameObject_7 = e.GameObject;
      //relay event to nodes
      Relay_OnEnterTrigger_7( );
   }
   
   void Instance_OnExitTrigger_7(object o, uScript_Trigger2D_NoStay.TriggerEventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      event_UnityEngine_GameObject_GameObject_7 = e.GameObject;
      //relay event to nodes
      Relay_OnExitTrigger_7( );
   }
   
   void Relay_CompareTag_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("a66dbe8f-745b-430c-a8f0-fd1b7fe67ea7", "UnityEngine_Transform", Relay_CompareTag_1)) return; 
         {
            {
            }
            {
            }
         }
         {
            UnityEngine.Transform component;
            component = local_0_UnityEngine_GameObject.GetComponent<UnityEngine.Transform>();
            if ( null != component )
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_1 = component.CompareTag(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_tag_1);
            }
         }
         local_Is_Player__System_Boolean = method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_1;
         Relay_In_3();
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Hazard.uscript at UnityEngine.Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_3()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("edad6089-6d3c-49fd-861f-9ac297e355ec", "Compare_Bool", Relay_In_3)) return; 
         {
            {
               logic_uScriptCon_CompareBool_Bool_3 = local_Is_Player__System_Boolean;
               
            }
         }
         logic_uScriptCon_CompareBool_uScriptCon_CompareBool_3.In(logic_uScriptCon_CompareBool_Bool_3);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptCon_CompareBool_uScriptCon_CompareBool_3.True;
         
         if ( test_0 == true )
         {
            Relay_True_4();
            Relay_In_9();
            Relay_In_10();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Hazard.uscript at Compare Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_True_4()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("5e04da6c-736f-4bf9-bfa6-a4178e975aac", "Set_Bool", Relay_True_4)) return; 
         {
            {
            }
         }
         logic_uScriptAct_SetBool_uScriptAct_SetBool_4.True(out logic_uScriptAct_SetBool_Target_4);
         property_Player_Lost__Detox_ScriptEditor_Parameter_Player_Lost__5 = logic_uScriptAct_SetBool_Target_4;
         property_Player_Lost__Detox_ScriptEditor_Parameter_Player_Lost__5_Set_Refresh( );
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Hazard.uscript at Set Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_False_4()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("5e04da6c-736f-4bf9-bfa6-a4178e975aac", "Set_Bool", Relay_False_4)) return; 
         {
            {
            }
         }
         logic_uScriptAct_SetBool_uScriptAct_SetBool_4.False(out logic_uScriptAct_SetBool_Target_4);
         property_Player_Lost__Detox_ScriptEditor_Parameter_Player_Lost__5 = logic_uScriptAct_SetBool_Target_4;
         property_Player_Lost__Detox_ScriptEditor_Parameter_Player_Lost__5_Set_Refresh( );
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Hazard.uscript at Set Bool.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_6()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         uScriptDebug.Log( "[Detox.ScriptEditor.LogicNode] Detecting trigger", uScriptDebug.Type.Message);
         if (true == CheckDebugBreak("c6920fb9-5ec3-4fdf-ae51-573f1cb3f92f", "Log", Relay_In_6)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_Log_uScriptAct_Log_6.In(logic_uScriptAct_Log_Prefix_6, logic_uScriptAct_Log_Target_6, logic_uScriptAct_Log_Postfix_6);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Hazard.uscript at Log.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_OnEnterTrigger_7()
   {
      if (true == CheckDebugBreak("1817c1a7-4bb7-4f0e-9f57-a613f207cd33", "Trigger_Event__2D____No_Stay", Relay_OnEnterTrigger_7)) return; 
      local_0_UnityEngine_GameObject = event_UnityEngine_GameObject_GameObject_7;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_0_UnityEngine_GameObject_previous != local_0_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_0_UnityEngine_GameObject_previous = local_0_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
      Relay_CompareTag_1();
      Relay_In_6();
   }
   
   void Relay_OnExitTrigger_7()
   {
      if (true == CheckDebugBreak("1817c1a7-4bb7-4f0e-9f57-a613f207cd33", "Trigger_Event__2D____No_Stay", Relay_OnExitTrigger_7)) return; 
      local_0_UnityEngine_GameObject = event_UnityEngine_GameObject_GameObject_7;
      {
         //if our game object reference was changed then we need to reset event listeners
         if ( local_0_UnityEngine_GameObject_previous != local_0_UnityEngine_GameObject || false == m_RegisteredForEvents )
         {
            //tear down old listeners
            
            local_0_UnityEngine_GameObject_previous = local_0_UnityEngine_GameObject;
            
            //setup new listeners
         }
      }
   }
   
   void Relay_In_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         uScriptDebug.Log( "[Detox.ScriptEditor.LogicNode] Object is Player", uScriptDebug.Type.Message);
         if (true == CheckDebugBreak("a4673fe4-14b0-43ba-89a6-4a81648ebddd", "Log", Relay_In_9)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_Log_uScriptAct_Log_9.In(logic_uScriptAct_Log_Prefix_9, logic_uScriptAct_Log_Target_9, logic_uScriptAct_Log_Postfix_9);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Hazard.uscript at Log.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_10()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         uScriptDebug.Log( "[Detox.ScriptEditor.LogicNode] Object is not Player", uScriptDebug.Type.Message);
         if (true == CheckDebugBreak("83582b26-2e29-45f9-8467-dab070a1e637", "Log", Relay_In_10)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_Log_uScriptAct_Log_10.In(logic_uScriptAct_Log_Prefix_10, logic_uScriptAct_Log_Target_10, logic_uScriptAct_Log_Postfix_10);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Hazard.uscript at Log.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Hazard.uscript:0", local_0_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "47574778-6ef6-4745-a605-41e88498223e", local_0_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Hazard.uscript:Is Player?", local_Is_Player__System_Boolean);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "7b38e985-fc7a-4979-aec2-70770507aeab", local_Is_Player__System_Boolean);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "f904d817-4ee7-404b-b86b-42d1c50fe077", property_Player_Lost__Detox_ScriptEditor_Parameter_Player_Lost__5_Get_Refresh());
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
