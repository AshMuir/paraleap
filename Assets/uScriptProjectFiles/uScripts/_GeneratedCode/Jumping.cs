//uScript Generated Code - Build 1.0.3072
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class Jumping : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public UnityEngine.Vector3 Jump_Force = new Vector3( (float)0, (float)5, (float)0 );
   UnityEngine.Rigidbody local_4_UnityEngine_Rigidbody = default(UnityEngine.Rigidbody);
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_3 = null;
   UnityEngine.GameObject owner_Connection_6 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_1 = UnityEngine.KeyCode.Space;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_1 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_1 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_1 = true;
   //pointer to script instanced logic node
   uScriptAct_GetRigidBody logic_uScriptAct_GetRigidBody_uScriptAct_GetRigidBody_2 = new uScriptAct_GetRigidBody( );
   UnityEngine.GameObject logic_uScriptAct_GetRigidBody_Target_2 = default(UnityEngine.GameObject);
   UnityEngine.Rigidbody logic_uScriptAct_GetRigidBody_rigidBody_2;
   bool logic_uScriptAct_GetRigidBody_Out_2 = true;
   //pointer to script instanced logic node
   uScriptAct_AddForce logic_uScriptAct_AddForce_uScriptAct_AddForce_5 = new uScriptAct_AddForce( );
   UnityEngine.GameObject logic_uScriptAct_AddForce_Target_5 = default(UnityEngine.GameObject);
   UnityEngine.Vector3 logic_uScriptAct_AddForce_Force_5 = new Vector3( );
   System.Single logic_uScriptAct_AddForce_Scale_5 = (float) 0;
   System.Boolean logic_uScriptAct_AddForce_UseForceMode_5 = (bool) false;
   UnityEngine.ForceMode logic_uScriptAct_AddForce_ForceModeType_5 = UnityEngine.ForceMode.Impulse;
   bool logic_uScriptAct_AddForce_Out_5 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_3 || false == m_RegisteredForEvents )
      {
         owner_Connection_3 = parentGameObject;
      }
      if ( null == owner_Connection_6 || false == m_RegisteredForEvents )
      {
         owner_Connection_6 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Input component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Input>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Input>();
               }
               if ( null != component )
               {
                  component.KeyEvent += Instance_KeyEvent_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Input component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Input>();
            if ( null != component )
            {
               component.KeyEvent -= Instance_KeyEvent_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1.SetParent(g);
      logic_uScriptAct_GetRigidBody_uScriptAct_GetRigidBody_2.SetParent(g);
      logic_uScriptAct_AddForce_uScriptAct_AddForce_5.SetParent(g);
      owner_Connection_3 = parentGameObject;
      owner_Connection_6 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_KeyEvent_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_KeyEvent_0( );
   }
   
   void Relay_KeyEvent_0()
   {
      if (true == CheckDebugBreak("c3d80993-c18b-4a49-a236-a313f856b9a5", "Input_Events", Relay_KeyEvent_0)) return; 
      Relay_In_1();
   }
   
   void Relay_In_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("4cff439b-efc9-47ea-8a01-5c8e950ea04f", "Input_Events_Filter", Relay_In_1)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1.In(logic_uScriptAct_OnInputEventFilter_KeyCode_1);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1.KeyDown;
         
         if ( test_0 == true )
         {
            Relay_In_5();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Jumping.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("c6bf0443-7063-47d9-9f5d-2195a9935f80", "Get_Rigidbody", Relay_In_2)) return; 
         {
            {
               logic_uScriptAct_GetRigidBody_Target_2 = owner_Connection_3;
               
            }
            {
            }
         }
         logic_uScriptAct_GetRigidBody_uScriptAct_GetRigidBody_2.In(logic_uScriptAct_GetRigidBody_Target_2, out logic_uScriptAct_GetRigidBody_rigidBody_2);
         local_4_UnityEngine_Rigidbody = logic_uScriptAct_GetRigidBody_rigidBody_2;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Jumping.uscript at Get Rigidbody.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_5()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("731f62ca-ae02-4616-a6d2-4d617610e9e5", "Add_Force", Relay_In_5)) return; 
         {
            {
               logic_uScriptAct_AddForce_Target_5 = owner_Connection_6;
               
            }
            {
               logic_uScriptAct_AddForce_Force_5 = Jump_Force;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_AddForce_uScriptAct_AddForce_5.In(logic_uScriptAct_AddForce_Target_5, logic_uScriptAct_AddForce_Force_5, logic_uScriptAct_AddForce_Scale_5, logic_uScriptAct_AddForce_UseForceMode_5, logic_uScriptAct_AddForce_ForceModeType_5);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript Jumping.uscript at Add Force.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Jumping.uscript:4", local_4_UnityEngine_Rigidbody);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "b256bbed-0671-4eff-b438-3a5875144e81", local_4_UnityEngine_Rigidbody);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "Jumping.uscript:Jump Force", Jump_Force);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "7f861021-c405-40ef-b397-52de8f71fa96", Jump_Force);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
