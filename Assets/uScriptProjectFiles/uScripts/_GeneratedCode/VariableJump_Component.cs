//uScript Generated Code - Build 1.0.3072
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// This is the component script that you should assign to GameObjects to use this graph on them. Use the uScript/Graphs section of Unity's "Component" menu to assign this graph to a selected GameObject.

[AddComponentMenu("uScript/Graphs/VariableJump")]
public class VariableJump_Component : uScriptCode
{
   #pragma warning disable 414
   public VariableJump ExposedVariables = new VariableJump( ); 
   #pragma warning restore 414
   
   public System.Single MinJumpHeight { get { return ExposedVariables.MinJumpHeight; } set { ExposedVariables.MinJumpHeight = value; } } 
   public System.Single MaxJumpHeight { get { return ExposedVariables.MaxJumpHeight; } set { ExposedVariables.MaxJumpHeight = value; } } 
   public System.Single TimeToApex { get { return ExposedVariables.TimeToApex; } set { ExposedVariables.TimeToApex = value; } } 
   public UnityEngine.GameObject RaycastPoint { get { return ExposedVariables.RaycastPoint; } set { ExposedVariables.RaycastPoint = value; } } 
   public System.Boolean IsGrounded { get { return ExposedVariables.IsGrounded; } set { ExposedVariables.IsGrounded = value; } } 
   
   void Awake( )
   {
      #if !(UNITY_FLASH)
      useGUILayout = false;
      #endif
      ExposedVariables.Awake( );
      ExposedVariables.SetParent( this.gameObject );
      if ( "1.PLE" != uScript_MasterComponent.Version )
      {
         uScriptDebug.Log( "The generated code is not compatible with your current uScript Runtime " + uScript_MasterComponent.Version, uScriptDebug.Type.Error );
         ExposedVariables = null;
         UnityEngine.Debug.Break();
      }
   }
   void Start( )
   {
      ExposedVariables.Start( );
   }
   void OnEnable( )
   {
      ExposedVariables.OnEnable( );
   }
   void OnDisable( )
   {
      ExposedVariables.OnDisable( );
   }
   void Update( )
   {
      ExposedVariables.Update( );
   }
   void OnDestroy( )
   {
      ExposedVariables.OnDestroy( );
   }
   #if UNITY_EDITOR
      void OnDrawGizmos( )
      {
      }
   #endif
}
