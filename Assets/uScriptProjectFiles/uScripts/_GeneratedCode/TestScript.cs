//uScript Generated Code - Build 1.0.3072
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("", "")]
public class TestScript : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public UnityEngine.Vector3 Jump_Force = new Vector3( (float)0, (float)5, (float)0 );
   System.Single local_13_System_Single = (float) 0;
   UnityEngine.GameObject local_17_UnityEngine_GameObject = default(UnityEngine.GameObject);
   UnityEngine.GameObject local_17_UnityEngine_GameObject_previous = null;
   System.Single local_18_System_Single = (float) 0;
   UnityEngine.Vector3 local_19_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_20_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_28_System_Single = (float) -0.01;
   System.Single local_32_System_Single = (float) 0.01;
   System.Single local_33_System_Single = (float) 0;
   System.Single local_34_System_Single = (float) 0;
   public System.Single MoveSpeed = (float) 10;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_9 = null;
   UnityEngine.GameObject owner_Connection_10 = null;
   UnityEngine.GameObject owner_Connection_11 = null;
   UnityEngine.GameObject owner_Connection_12 = null;
   UnityEngine.GameObject owner_Connection_24 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_0 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_0 = UnityEngine.KeyCode.W;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_0 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_0 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_0 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_2 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_2 = UnityEngine.KeyCode.A;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_2 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_2 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_2 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_3 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_3 = UnityEngine.KeyCode.D;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_3 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_3 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_3 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_4 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_4 = UnityEngine.KeyCode.S;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_4 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_4 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_4 = true;
   //pointer to script instanced logic node
   uScriptAct_LookAt logic_uScriptAct_LookAt_uScriptAct_LookAt_14 = new uScriptAct_LookAt( );
   UnityEngine.GameObject[] logic_uScriptAct_LookAt_Target_14 = new UnityEngine.GameObject[] {};
   System.Object logic_uScriptAct_LookAt_Focus_14 = "";
   System.Single logic_uScriptAct_LookAt_time_14 = (float) 0;
   uScriptAct_LookAt.LockAxis logic_uScriptAct_LookAt_lockAxis_14 = uScriptAct_LookAt.LockAxis.None;
   bool logic_uScriptAct_LookAt_Out_14 = true;
   //pointer to script instanced logic node
   uScriptAct_GetComponentsVector3 logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_15 = new uScriptAct_GetComponentsVector3( );
   UnityEngine.Vector3 logic_uScriptAct_GetComponentsVector3_InputVector3_15 = new Vector3( );
   System.Single logic_uScriptAct_GetComponentsVector3_X_15;
   System.Single logic_uScriptAct_GetComponentsVector3_Y_15;
   System.Single logic_uScriptAct_GetComponentsVector3_Z_15;
   bool logic_uScriptAct_GetComponentsVector3_Out_15 = true;
   //pointer to script instanced logic node
   uScriptAct_SetComponentsVector3 logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_16 = new uScriptAct_SetComponentsVector3( );
   System.Single logic_uScriptAct_SetComponentsVector3_X_16 = (float) 0;
   System.Single logic_uScriptAct_SetComponentsVector3_Y_16 = (float) 0;
   System.Single logic_uScriptAct_SetComponentsVector3_Z_16 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_SetComponentsVector3_OutputVector3_16;
   bool logic_uScriptAct_SetComponentsVector3_Out_16 = true;
   //pointer to script instanced logic node
   uScriptAct_RaycastFromCursor logic_uScriptAct_RaycastFromCursor_uScriptAct_RaycastFromCursor_21 = new uScriptAct_RaycastFromCursor( );
   UnityEngine.Camera logic_uScriptAct_RaycastFromCursor_Camera_21 = default(UnityEngine.Camera);
   System.Single logic_uScriptAct_RaycastFromCursor_Distance_21 = (float) 1000;
   UnityEngine.LayerMask logic_uScriptAct_RaycastFromCursor_layerMask_21 = 1;
   System.Boolean logic_uScriptAct_RaycastFromCursor_include_21 = (bool) true;
   System.Boolean logic_uScriptAct_RaycastFromCursor_showRay_21 = (bool) false;
   UnityEngine.GameObject logic_uScriptAct_RaycastFromCursor_HitObject_21;
   System.Single logic_uScriptAct_RaycastFromCursor_HitDistance_21;
   UnityEngine.Vector3 logic_uScriptAct_RaycastFromCursor_HitLocation_21;
   UnityEngine.Vector3 logic_uScriptAct_RaycastFromCursor_HitNormal_21;
   bool logic_uScriptAct_RaycastFromCursor_NotObstructed_21 = true;
   bool logic_uScriptAct_RaycastFromCursor_Obstructed_21 = true;
   //pointer to script instanced logic node
   uScriptAct_AddForce logic_uScriptAct_AddForce_uScriptAct_AddForce_23 = new uScriptAct_AddForce( );
   UnityEngine.GameObject logic_uScriptAct_AddForce_Target_23 = default(UnityEngine.GameObject);
   UnityEngine.Vector3 logic_uScriptAct_AddForce_Force_23 = new Vector3( );
   System.Single logic_uScriptAct_AddForce_Scale_23 = (float) 0;
   System.Boolean logic_uScriptAct_AddForce_UseForceMode_23 = (bool) false;
   UnityEngine.ForceMode logic_uScriptAct_AddForce_ForceModeType_23 = UnityEngine.ForceMode.Impulse;
   bool logic_uScriptAct_AddForce_Out_23 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_25 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_25 = UnityEngine.KeyCode.Space;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_25 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_25 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_25 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_27 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_27 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_27 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_27;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_27;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_27 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_30 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_30 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_30 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_30;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_30;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_30 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_1 = default(UnityEngine.GameObject);
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_31 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_5 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_y_5 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_5 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_6 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_y_6 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_6 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_7 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_y_7 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_7 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_8 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_y_8 = (float) 0;
   System.Single method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_8 = (float) 0;
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == logic_uScriptAct_RaycastFromCursor_Camera_21 || false == m_RegisteredForEvents )
      {
         GameObject gameObject = GameObject.Find( "/Main Camera" );
         if ( null != gameObject )
         {
            logic_uScriptAct_RaycastFromCursor_Camera_21 = gameObject.GetComponent<UnityEngine.Camera>();
         }
      }
      if ( null == local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_17_UnityEngine_GameObject = GameObject.Find( "Sphere" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == owner_Connection_9 || false == m_RegisteredForEvents )
      {
         owner_Connection_9 = parentGameObject;
      }
      if ( null == owner_Connection_10 || false == m_RegisteredForEvents )
      {
         owner_Connection_10 = parentGameObject;
      }
      if ( null == owner_Connection_11 || false == m_RegisteredForEvents )
      {
         owner_Connection_11 = parentGameObject;
      }
      if ( null == owner_Connection_12 || false == m_RegisteredForEvents )
      {
         owner_Connection_12 = parentGameObject;
      }
      if ( null == owner_Connection_24 || false == m_RegisteredForEvents )
      {
         owner_Connection_24 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_1 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_1 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_1 )
         {
            {
               uScript_Input component = event_UnityEngine_GameObject_Instance_1.GetComponent<uScript_Input>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_1.AddComponent<uScript_Input>();
               }
               if ( null != component )
               {
                  component.KeyEvent += Instance_KeyEvent_1;
               }
            }
         }
      }
      if ( null == event_UnityEngine_GameObject_Instance_31 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_31 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_31 )
         {
            {
               uScript_Global component = event_UnityEngine_GameObject_Instance_31.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_31.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_31;
                  component.uScriptLateStart += Instance_uScriptLateStart_31;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_1 )
      {
         {
            uScript_Input component = event_UnityEngine_GameObject_Instance_1.GetComponent<uScript_Input>();
            if ( null != component )
            {
               component.KeyEvent -= Instance_KeyEvent_1;
            }
         }
      }
      if ( null != event_UnityEngine_GameObject_Instance_31 )
      {
         {
            uScript_Global component = event_UnityEngine_GameObject_Instance_31.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_31;
               component.uScriptLateStart -= Instance_uScriptLateStart_31;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_0.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_2.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_3.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_4.SetParent(g);
      logic_uScriptAct_LookAt_uScriptAct_LookAt_14.SetParent(g);
      logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_15.SetParent(g);
      logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_16.SetParent(g);
      logic_uScriptAct_RaycastFromCursor_uScriptAct_RaycastFromCursor_21.SetParent(g);
      logic_uScriptAct_AddForce_uScriptAct_AddForce_23.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_25.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_27.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_30.SetParent(g);
      owner_Connection_9 = parentGameObject;
      owner_Connection_10 = parentGameObject;
      owner_Connection_11 = parentGameObject;
      owner_Connection_12 = parentGameObject;
      owner_Connection_24 = parentGameObject;
   }
   public void Awake()
   {
      
      logic_uScriptAct_LookAt_uScriptAct_LookAt_14.Finished += uScriptAct_LookAt_Finished_14;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_LookAt_uScriptAct_LookAt_14.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_LookAt_uScriptAct_LookAt_14.Finished -= uScriptAct_LookAt_Finished_14;
   }
   
   void Instance_KeyEvent_1(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_KeyEvent_1( );
   }
   
   void Instance_uScriptStart_31(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_31( );
   }
   
   void Instance_uScriptLateStart_31(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_31( );
   }
   
   void uScriptAct_LookAt_Finished_14(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_14( );
   }
   
   void Relay_In_0()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("4ef95462-9bb3-4c23-b75e-d90b50e689bc", "Input_Events_Filter", Relay_In_0)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_0.In(logic_uScriptAct_OnInputEventFilter_KeyCode_0);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_0.KeyHeld;
         
         if ( test_0 == true )
         {
            Relay_Translate_5();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_KeyEvent_1()
   {
      if (true == CheckDebugBreak("945b86d9-a2c6-473c-a0f2-8c07cf1468c0", "Input_Events", Relay_KeyEvent_1)) return; 
      Relay_In_0();
      Relay_In_3();
      Relay_In_2();
      Relay_In_4();
      Relay_In_25();
   }
   
   void Relay_In_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("61b967a5-686a-4f4d-abab-2f255c8466d1", "Input_Events_Filter", Relay_In_2)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_2.In(logic_uScriptAct_OnInputEventFilter_KeyCode_2);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_2.KeyHeld;
         
         if ( test_0 == true )
         {
            Relay_Translate_8();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_3()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("4a8450f3-99d3-48a4-b22f-11fa230cdec7", "Input_Events_Filter", Relay_In_3)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_3.In(logic_uScriptAct_OnInputEventFilter_KeyCode_3);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_3.KeyHeld;
         
         if ( test_0 == true )
         {
            Relay_Translate_6();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_4()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("cb75e1d0-5f4a-42e4-b7a5-6907a8e0e1ba", "Input_Events_Filter", Relay_In_4)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_4.In(logic_uScriptAct_OnInputEventFilter_KeyCode_4);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_4.KeyHeld;
         
         if ( test_0 == true )
         {
            Relay_Translate_7();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Translate_5()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("83c07a29-ab97-4d86-96ca-97153935efb6", "UnityEngine_Transform", Relay_Translate_5)) return; 
         {
            {
            }
            {
            }
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_5 = local_34_System_Single;
               
            }
         }
         {
            UnityEngine.Transform component;
            component = owner_Connection_9.GetComponent<UnityEngine.Transform>();
            if ( null != component )
            {
               component.Translate(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_5, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_y_5, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_5);
            }
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at UnityEngine.Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Translate_6()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("b7e75cba-8a13-4392-8316-ca1987848e78", "UnityEngine_Transform", Relay_Translate_6)) return; 
         {
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_6 = local_34_System_Single;
               
            }
            {
            }
            {
            }
         }
         {
            UnityEngine.Transform component;
            component = owner_Connection_10.GetComponent<UnityEngine.Transform>();
            if ( null != component )
            {
               component.Translate(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_6, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_y_6, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_6);
            }
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at UnityEngine.Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Translate_7()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("3dd487dd-a69a-4c68-b65b-4a4b16a62b5f", "UnityEngine_Transform", Relay_Translate_7)) return; 
         {
            {
            }
            {
            }
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_7 = local_33_System_Single;
               
            }
         }
         {
            UnityEngine.Transform component;
            component = owner_Connection_12.GetComponent<UnityEngine.Transform>();
            if ( null != component )
            {
               component.Translate(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_7, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_y_7, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_7);
            }
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at UnityEngine.Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Translate_8()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("964cfa6e-7a54-4a13-b70f-3ef3a1daa17c", "UnityEngine_Transform", Relay_Translate_8)) return; 
         {
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_8 = local_33_System_Single;
               
            }
            {
            }
            {
            }
         }
         {
            UnityEngine.Transform component;
            component = owner_Connection_11.GetComponent<UnityEngine.Transform>();
            if ( null != component )
            {
               component.Translate(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_x_8, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_y_8, method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_z_8);
            }
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at UnityEngine.Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_14()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("1e43f584-c5f0-4736-b006-6da88c0789cd", "Look_At", Relay_Finished_14)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at Look At.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_14()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("1e43f584-c5f0-4736-b006-6da88c0789cd", "Look_At", Relay_In_14)) return; 
         {
            {
               int index = 0;
               {
                  //if our game object reference was changed then we need to reset event listeners
                  if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
                  {
                     //tear down old listeners
                     
                     local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
                     
                     //setup new listeners
                  }
               }
               if ( logic_uScriptAct_LookAt_Target_14.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_LookAt_Target_14, index + 1);
               }
               logic_uScriptAct_LookAt_Target_14[ index++ ] = local_17_UnityEngine_GameObject;
               
            }
            {
               logic_uScriptAct_LookAt_Focus_14 = local_19_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_LookAt_uScriptAct_LookAt_14.In(logic_uScriptAct_LookAt_Target_14, logic_uScriptAct_LookAt_Focus_14, logic_uScriptAct_LookAt_time_14, logic_uScriptAct_LookAt_lockAxis_14);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at Look At.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_15()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("cb7fb51d-7d00-487b-b103-37d14d1d812e", "Get_Components__Vector3_", Relay_In_15)) return; 
         {
            {
               logic_uScriptAct_GetComponentsVector3_InputVector3_15 = local_20_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_15.In(logic_uScriptAct_GetComponentsVector3_InputVector3_15, out logic_uScriptAct_GetComponentsVector3_X_15, out logic_uScriptAct_GetComponentsVector3_Y_15, out logic_uScriptAct_GetComponentsVector3_Z_15);
         local_13_System_Single = logic_uScriptAct_GetComponentsVector3_X_15;
         local_18_System_Single = logic_uScriptAct_GetComponentsVector3_Z_15;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_15.Out;
         
         if ( test_0 == true )
         {
            Relay_In_16();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at Get Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_16()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("9a0e819a-a771-4a01-a26c-8e8ff33116cd", "Set_Components__Vector3_", Relay_In_16)) return; 
         {
            {
               logic_uScriptAct_SetComponentsVector3_X_16 = local_13_System_Single;
               
            }
            {
            }
            {
               logic_uScriptAct_SetComponentsVector3_Z_16 = local_18_System_Single;
               
            }
            {
            }
         }
         logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_16.In(logic_uScriptAct_SetComponentsVector3_X_16, logic_uScriptAct_SetComponentsVector3_Y_16, logic_uScriptAct_SetComponentsVector3_Z_16, out logic_uScriptAct_SetComponentsVector3_OutputVector3_16);
         local_19_UnityEngine_Vector3 = logic_uScriptAct_SetComponentsVector3_OutputVector3_16;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_16.Out;
         
         if ( test_0 == true )
         {
            Relay_In_14();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at Set Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_21()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("14847e70-a276-421f-bb7e-e1c3fafd7a14", "Raycast_From_Mouse_Cursor", Relay_In_21)) return; 
         {
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_RaycastFromCursor_uScriptAct_RaycastFromCursor_21.In(logic_uScriptAct_RaycastFromCursor_Camera_21, logic_uScriptAct_RaycastFromCursor_Distance_21, logic_uScriptAct_RaycastFromCursor_layerMask_21, logic_uScriptAct_RaycastFromCursor_include_21, logic_uScriptAct_RaycastFromCursor_showRay_21, out logic_uScriptAct_RaycastFromCursor_HitObject_21, out logic_uScriptAct_RaycastFromCursor_HitDistance_21, out logic_uScriptAct_RaycastFromCursor_HitLocation_21, out logic_uScriptAct_RaycastFromCursor_HitNormal_21);
         local_20_UnityEngine_Vector3 = logic_uScriptAct_RaycastFromCursor_HitLocation_21;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_RaycastFromCursor_uScriptAct_RaycastFromCursor_21.Obstructed;
         
         if ( test_0 == true )
         {
            Relay_In_15();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at Raycast From Mouse Cursor.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_23()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("f9ee0545-6055-4014-b634-1d52b81d9293", "Add_Force", Relay_In_23)) return; 
         {
            {
               logic_uScriptAct_AddForce_Target_23 = owner_Connection_24;
               
            }
            {
               logic_uScriptAct_AddForce_Force_23 = Jump_Force;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_AddForce_uScriptAct_AddForce_23.In(logic_uScriptAct_AddForce_Target_23, logic_uScriptAct_AddForce_Force_23, logic_uScriptAct_AddForce_Scale_23, logic_uScriptAct_AddForce_UseForceMode_23, logic_uScriptAct_AddForce_ForceModeType_23);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at Add Force.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_25()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("c21d825a-d32f-41f9-b838-fccd324c72b5", "Input_Events_Filter", Relay_In_25)) return; 
         {
            {
            }
         }
         logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_25.In(logic_uScriptAct_OnInputEventFilter_KeyCode_25);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_25.KeyDown;
         
         if ( test_0 == true )
         {
            Relay_In_23();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at Input Events Filter.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_27()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("5c197655-43b0-46ff-ab14-4064906ebe08", "Multiply_Float", Relay_In_27)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_27 = local_32_System_Single;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_27 = MoveSpeed;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_27.In(logic_uScriptAct_MultiplyFloat_v2_A_27, logic_uScriptAct_MultiplyFloat_v2_B_27, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_27, out logic_uScriptAct_MultiplyFloat_v2_IntResult_27);
         local_34_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_27;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_30()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("2335f16f-0c5c-4e5d-b321-74b9403865cf", "Multiply_Float", Relay_In_30)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_30 = local_28_System_Single;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_30 = MoveSpeed;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_30.In(logic_uScriptAct_MultiplyFloat_v2_A_30, logic_uScriptAct_MultiplyFloat_v2_B_30, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_30, out logic_uScriptAct_MultiplyFloat_v2_IntResult_30);
         local_33_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_30;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript TestScript.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_uScriptStart_31()
   {
      if (true == CheckDebugBreak("a8755fc7-120d-43db-ae5b-fb0d9e730d5d", "uScript_Events", Relay_uScriptStart_31)) return; 
      Relay_In_30();
      Relay_In_27();
   }
   
   void Relay_uScriptLateStart_31()
   {
      if (true == CheckDebugBreak("a8755fc7-120d-43db-ae5b-fb0d9e730d5d", "uScript_Events", Relay_uScriptLateStart_31)) return; 
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "TestScript.uscript:13", local_13_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "32d5c378-25f4-4085-9ce0-ab9469a9b1a3", local_13_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "TestScript.uscript:17", local_17_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "518bfd50-54a8-4687-a5f9-41f27e1ffed7", local_17_UnityEngine_GameObject);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "TestScript.uscript:18", local_18_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "690c343f-f4bb-4a9e-a5c5-b51f0da79bec", local_18_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "TestScript.uscript:19", local_19_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "9be894d4-4f92-47ba-bb83-a8e83377843a", local_19_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "TestScript.uscript:20", local_20_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "e1f43efa-6d31-4433-89f7-1523829a8eba", local_20_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "TestScript.uscript:Jump Force", Jump_Force);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "8a47ef93-6ebe-45ef-abca-f15a78b9da1f", Jump_Force);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "TestScript.uscript:28", local_28_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "332817f2-c7d8-431a-9f38-6c30811813b9", local_28_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "TestScript.uscript:MoveSpeed", MoveSpeed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "8bdc9e28-61fd-43de-a8e5-150ffdf07a13", MoveSpeed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "TestScript.uscript:32", local_32_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "7afb8485-a123-489c-a272-1dc7be0c2ea6", local_32_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "TestScript.uscript:33", local_33_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "996f5fb8-8b0e-4a53-85e7-d2543f901404", local_33_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "TestScript.uscript:34", local_34_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "2c551885-a5d8-43dc-8175-3eb9852157c4", local_34_System_Single);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
