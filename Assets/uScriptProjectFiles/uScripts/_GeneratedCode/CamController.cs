//uScript Generated Code - Build 1.0.3072
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class CamController : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public System.Single Cam_Distance = (float) -10;
   public System.Single Cam_Move_Speed = (float) 5;
   System.Single local_10_System_Single = (float) 0;
   System.Single local_11_System_Single = (float) 0;
   UnityEngine.Vector3 local_3_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_6_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Transform local_7_UnityEngine_Transform = default(UnityEngine.Transform);
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_4 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_MoveToLocation logic_uScriptAct_MoveToLocation_uScriptAct_MoveToLocation_1 = new uScriptAct_MoveToLocation( );
   UnityEngine.GameObject[] logic_uScriptAct_MoveToLocation_targetArray_1 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_MoveToLocation_location_1 = new Vector3( );
   System.Boolean logic_uScriptAct_MoveToLocation_asOffset_1 = (bool) false;
   System.Single logic_uScriptAct_MoveToLocation_totalTime_1 = (float) 0;
   bool logic_uScriptAct_MoveToLocation_Out_1 = true;
   bool logic_uScriptAct_MoveToLocation_Cancelled_1 = true;
   //pointer to script instanced logic node
   uScriptAct_GetPositionFromTransform logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_2 = new uScriptAct_GetPositionFromTransform( );
   UnityEngine.Transform logic_uScriptAct_GetPositionFromTransform_target_2 = default(UnityEngine.Transform);
   System.Boolean logic_uScriptAct_GetPositionFromTransform_getLocal_2 = (bool) false;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_position_2;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_forward_2;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_right_2;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_up_2;
   UnityEngine.Matrix4x4 logic_uScriptAct_GetPositionFromTransform_worldMatrix_2;
   bool logic_uScriptAct_GetPositionFromTransform_Out_2 = true;
   //pointer to script instanced logic node
   uScriptAct_GetComponentsVector3 logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_8 = new uScriptAct_GetComponentsVector3( );
   UnityEngine.Vector3 logic_uScriptAct_GetComponentsVector3_InputVector3_8 = new Vector3( );
   System.Single logic_uScriptAct_GetComponentsVector3_X_8;
   System.Single logic_uScriptAct_GetComponentsVector3_Y_8;
   System.Single logic_uScriptAct_GetComponentsVector3_Z_8;
   bool logic_uScriptAct_GetComponentsVector3_Out_8 = true;
   //pointer to script instanced logic node
   uScriptAct_SetComponentsVector3 logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_9 = new uScriptAct_SetComponentsVector3( );
   System.Single logic_uScriptAct_SetComponentsVector3_X_9 = (float) 0;
   System.Single logic_uScriptAct_SetComponentsVector3_Y_9 = (float) 0;
   System.Single logic_uScriptAct_SetComponentsVector3_Z_9 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_SetComponentsVector3_OutputVector3_9;
   bool logic_uScriptAct_SetComponentsVector3_Out_9 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_7_UnityEngine_Transform || false == m_RegisteredForEvents )
      {
         GameObject gameObject = GameObject.Find( "Cam Target" );
         if ( null != gameObject )
         {
            local_7_UnityEngine_Transform = gameObject.GetComponent<UnityEngine.Transform>();
         }
      }
      if ( null == owner_Connection_4 || false == m_RegisteredForEvents )
      {
         owner_Connection_4 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_0;
                  component.OnLateUpdate += Instance_OnLateUpdate_0;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_0;
               component.OnLateUpdate -= Instance_OnLateUpdate_0;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_MoveToLocation_uScriptAct_MoveToLocation_1.SetParent(g);
      logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_2.SetParent(g);
      logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_8.SetParent(g);
      logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_9.SetParent(g);
      owner_Connection_4 = parentGameObject;
   }
   public void Awake()
   {
      
      logic_uScriptAct_MoveToLocation_uScriptAct_MoveToLocation_1.Finished += uScriptAct_MoveToLocation_Finished_1;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_MoveToLocation_uScriptAct_MoveToLocation_1.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_MoveToLocation_uScriptAct_MoveToLocation_1.Finished -= uScriptAct_MoveToLocation_Finished_1;
   }
   
   void Instance_OnUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_0( );
   }
   
   void Instance_OnLateUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_0( );
   }
   
   void Instance_OnFixedUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_0( );
   }
   
   void uScriptAct_MoveToLocation_Finished_1(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_1( );
   }
   
   void Relay_OnUpdate_0()
   {
      if (true == CheckDebugBreak("1bde8f25-3b47-4091-a109-e1af6f8ec220", "Global_Update", Relay_OnUpdate_0)) return; 
      Relay_In_2();
   }
   
   void Relay_OnLateUpdate_0()
   {
      if (true == CheckDebugBreak("1bde8f25-3b47-4091-a109-e1af6f8ec220", "Global_Update", Relay_OnLateUpdate_0)) return; 
   }
   
   void Relay_OnFixedUpdate_0()
   {
      if (true == CheckDebugBreak("1bde8f25-3b47-4091-a109-e1af6f8ec220", "Global_Update", Relay_OnFixedUpdate_0)) return; 
   }
   
   void Relay_Finished_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("ab000b85-f437-45fb-bb70-be997fd64475", "Move_To_Location", Relay_Finished_1)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CamController.uscript at Move To Location.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("ab000b85-f437-45fb-bb70-be997fd64475", "Move_To_Location", Relay_In_1)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_MoveToLocation_targetArray_1.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_MoveToLocation_targetArray_1, index + 1);
               }
               logic_uScriptAct_MoveToLocation_targetArray_1[ index++ ] = owner_Connection_4;
               
            }
            {
               logic_uScriptAct_MoveToLocation_location_1 = local_6_UnityEngine_Vector3;
               
            }
            {
            }
            {
               logic_uScriptAct_MoveToLocation_totalTime_1 = Cam_Move_Speed;
               
            }
         }
         logic_uScriptAct_MoveToLocation_uScriptAct_MoveToLocation_1.In(logic_uScriptAct_MoveToLocation_targetArray_1, logic_uScriptAct_MoveToLocation_location_1, logic_uScriptAct_MoveToLocation_asOffset_1, logic_uScriptAct_MoveToLocation_totalTime_1);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CamController.uscript at Move To Location.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Cancel_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("ab000b85-f437-45fb-bb70-be997fd64475", "Move_To_Location", Relay_Cancel_1)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_MoveToLocation_targetArray_1.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_MoveToLocation_targetArray_1, index + 1);
               }
               logic_uScriptAct_MoveToLocation_targetArray_1[ index++ ] = owner_Connection_4;
               
            }
            {
               logic_uScriptAct_MoveToLocation_location_1 = local_6_UnityEngine_Vector3;
               
            }
            {
            }
            {
               logic_uScriptAct_MoveToLocation_totalTime_1 = Cam_Move_Speed;
               
            }
         }
         logic_uScriptAct_MoveToLocation_uScriptAct_MoveToLocation_1.Cancel(logic_uScriptAct_MoveToLocation_targetArray_1, logic_uScriptAct_MoveToLocation_location_1, logic_uScriptAct_MoveToLocation_asOffset_1, logic_uScriptAct_MoveToLocation_totalTime_1);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CamController.uscript at Move To Location.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("f0baa7ca-9fa1-418f-83f7-9d009fa7b8a1", "Get_Position_From_Transform", Relay_In_2)) return; 
         {
            {
               logic_uScriptAct_GetPositionFromTransform_target_2 = local_7_UnityEngine_Transform;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_2.In(logic_uScriptAct_GetPositionFromTransform_target_2, logic_uScriptAct_GetPositionFromTransform_getLocal_2, out logic_uScriptAct_GetPositionFromTransform_position_2, out logic_uScriptAct_GetPositionFromTransform_forward_2, out logic_uScriptAct_GetPositionFromTransform_right_2, out logic_uScriptAct_GetPositionFromTransform_up_2, out logic_uScriptAct_GetPositionFromTransform_worldMatrix_2);
         local_3_UnityEngine_Vector3 = logic_uScriptAct_GetPositionFromTransform_position_2;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_2.Out;
         
         if ( test_0 == true )
         {
            Relay_In_8();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CamController.uscript at Get Position From Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_8()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("a443c4e0-3d4b-4675-b0b7-ac16b9a1c36e", "Get_Components__Vector3_", Relay_In_8)) return; 
         {
            {
               logic_uScriptAct_GetComponentsVector3_InputVector3_8 = local_3_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_8.In(logic_uScriptAct_GetComponentsVector3_InputVector3_8, out logic_uScriptAct_GetComponentsVector3_X_8, out logic_uScriptAct_GetComponentsVector3_Y_8, out logic_uScriptAct_GetComponentsVector3_Z_8);
         local_10_System_Single = logic_uScriptAct_GetComponentsVector3_X_8;
         local_11_System_Single = logic_uScriptAct_GetComponentsVector3_Y_8;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_8.Out;
         
         if ( test_0 == true )
         {
            Relay_In_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CamController.uscript at Get Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("ffd6e32c-7d06-425e-a3c2-82bdb9bdad08", "Set_Components__Vector3_", Relay_In_9)) return; 
         {
            {
               logic_uScriptAct_SetComponentsVector3_X_9 = local_10_System_Single;
               
            }
            {
               logic_uScriptAct_SetComponentsVector3_Y_9 = local_11_System_Single;
               
            }
            {
               logic_uScriptAct_SetComponentsVector3_Z_9 = Cam_Distance;
               
            }
            {
            }
         }
         logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_9.In(logic_uScriptAct_SetComponentsVector3_X_9, logic_uScriptAct_SetComponentsVector3_Y_9, logic_uScriptAct_SetComponentsVector3_Z_9, out logic_uScriptAct_SetComponentsVector3_OutputVector3_9);
         local_6_UnityEngine_Vector3 = logic_uScriptAct_SetComponentsVector3_OutputVector3_9;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_9.Out;
         
         if ( test_0 == true )
         {
            Relay_In_1();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CamController.uscript at Set Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CamController.uscript:3", local_3_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "377741bb-a837-4501-843d-7e8c523606d8", local_3_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CamController.uscript:Cam Move Speed", Cam_Move_Speed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "7e9a939c-6f6c-48b7-a018-bead898b697a", Cam_Move_Speed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CamController.uscript:6", local_6_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "8cee37a1-77c9-40b0-9e87-560190127ad1", local_6_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CamController.uscript:7", local_7_UnityEngine_Transform);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "945e7717-6b96-4ba8-be15-18ce2e12dbbf", local_7_UnityEngine_Transform);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CamController.uscript:10", local_10_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "1f1822cf-d8d1-4a25-bd25-a7985241e773", local_10_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CamController.uscript:11", local_11_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "fe827eac-8bb8-4af7-b717-c10663fa3c13", local_11_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CamController.uscript:Cam Distance", Cam_Distance);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "98f4f260-a090-44be-8c09-75959a86a17e", Cam_Distance);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
