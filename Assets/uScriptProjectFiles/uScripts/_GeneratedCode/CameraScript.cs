//uScript Generated Code - Build 1.0.3072
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class CameraScript : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   System.Single local_10_System_Single = (float) 4;
   System.Single local_11_System_Single = (float) 0;
   UnityEngine.Vector3 local_13_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_2_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Transform local_5_UnityEngine_Transform = default(UnityEngine.Transform);
   System.Single local_6_System_Single = (float) 0;
   System.Single local_8_System_Single = (float) 0;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_4 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_GetPositionFromTransform logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_1 = new uScriptAct_GetPositionFromTransform( );
   UnityEngine.Transform logic_uScriptAct_GetPositionFromTransform_target_1 = default(UnityEngine.Transform);
   System.Boolean logic_uScriptAct_GetPositionFromTransform_getLocal_1 = (bool) false;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_position_1;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_forward_1;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_right_1;
   UnityEngine.Vector3 logic_uScriptAct_GetPositionFromTransform_up_1;
   UnityEngine.Matrix4x4 logic_uScriptAct_GetPositionFromTransform_worldMatrix_1;
   bool logic_uScriptAct_GetPositionFromTransform_Out_1 = true;
   //pointer to script instanced logic node
   uScriptAct_MoveToLocation logic_uScriptAct_MoveToLocation_uScriptAct_MoveToLocation_3 = new uScriptAct_MoveToLocation( );
   UnityEngine.GameObject[] logic_uScriptAct_MoveToLocation_targetArray_3 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_MoveToLocation_location_3 = new Vector3( );
   System.Boolean logic_uScriptAct_MoveToLocation_asOffset_3 = (bool) false;
   System.Single logic_uScriptAct_MoveToLocation_totalTime_3 = (float) 0.2;
   bool logic_uScriptAct_MoveToLocation_Out_3 = true;
   bool logic_uScriptAct_MoveToLocation_Cancelled_3 = true;
   //pointer to script instanced logic node
   uScriptAct_GetComponentsVector3 logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_7 = new uScriptAct_GetComponentsVector3( );
   UnityEngine.Vector3 logic_uScriptAct_GetComponentsVector3_InputVector3_7 = new Vector3( );
   System.Single logic_uScriptAct_GetComponentsVector3_X_7;
   System.Single logic_uScriptAct_GetComponentsVector3_Y_7;
   System.Single logic_uScriptAct_GetComponentsVector3_Z_7;
   bool logic_uScriptAct_GetComponentsVector3_Out_7 = true;
   //pointer to script instanced logic node
   uScriptAct_SubtractFloat logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_9 = new uScriptAct_SubtractFloat( );
   System.Single logic_uScriptAct_SubtractFloat_A_9 = (float) 0;
   System.Single logic_uScriptAct_SubtractFloat_B_9 = (float) 0;
   System.Single logic_uScriptAct_SubtractFloat_FloatResult_9;
   System.Int32 logic_uScriptAct_SubtractFloat_IntResult_9;
   bool logic_uScriptAct_SubtractFloat_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_SetComponentsVector3 logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_12 = new uScriptAct_SetComponentsVector3( );
   System.Single logic_uScriptAct_SetComponentsVector3_X_12 = (float) 0;
   System.Single logic_uScriptAct_SetComponentsVector3_Y_12 = (float) 3;
   System.Single logic_uScriptAct_SetComponentsVector3_Z_12 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_SetComponentsVector3_OutputVector3_12;
   bool logic_uScriptAct_SetComponentsVector3_Out_12 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_5_UnityEngine_Transform || false == m_RegisteredForEvents )
      {
         GameObject gameObject = GameObject.Find( "/Sphere/CamPos" );
         if ( null != gameObject )
         {
            local_5_UnityEngine_Transform = gameObject.GetComponent<UnityEngine.Transform>();
         }
      }
      if ( null == owner_Connection_4 || false == m_RegisteredForEvents )
      {
         owner_Connection_4 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_0;
                  component.OnLateUpdate += Instance_OnLateUpdate_0;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_0;
               component.OnLateUpdate -= Instance_OnLateUpdate_0;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_1.SetParent(g);
      logic_uScriptAct_MoveToLocation_uScriptAct_MoveToLocation_3.SetParent(g);
      logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_7.SetParent(g);
      logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_9.SetParent(g);
      logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_12.SetParent(g);
      owner_Connection_4 = parentGameObject;
   }
   public void Awake()
   {
      
      logic_uScriptAct_MoveToLocation_uScriptAct_MoveToLocation_3.Finished += uScriptAct_MoveToLocation_Finished_3;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_MoveToLocation_uScriptAct_MoveToLocation_3.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_MoveToLocation_uScriptAct_MoveToLocation_3.Finished -= uScriptAct_MoveToLocation_Finished_3;
   }
   
   void Instance_OnUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_0( );
   }
   
   void Instance_OnLateUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_0( );
   }
   
   void Instance_OnFixedUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_0( );
   }
   
   void uScriptAct_MoveToLocation_Finished_3(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Finished_3( );
   }
   
   void Relay_OnUpdate_0()
   {
      if (true == CheckDebugBreak("c1269862-2a04-4bd7-acef-fa22e5acbae9", "Global_Update", Relay_OnUpdate_0)) return; 
      Relay_In_1();
   }
   
   void Relay_OnLateUpdate_0()
   {
      if (true == CheckDebugBreak("c1269862-2a04-4bd7-acef-fa22e5acbae9", "Global_Update", Relay_OnLateUpdate_0)) return; 
   }
   
   void Relay_OnFixedUpdate_0()
   {
      if (true == CheckDebugBreak("c1269862-2a04-4bd7-acef-fa22e5acbae9", "Global_Update", Relay_OnFixedUpdate_0)) return; 
   }
   
   void Relay_In_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("c3a5561d-622e-47f6-9592-269a0f8d0cb8", "Get_Position_From_Transform", Relay_In_1)) return; 
         {
            {
               logic_uScriptAct_GetPositionFromTransform_target_1 = local_5_UnityEngine_Transform;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_1.In(logic_uScriptAct_GetPositionFromTransform_target_1, logic_uScriptAct_GetPositionFromTransform_getLocal_1, out logic_uScriptAct_GetPositionFromTransform_position_1, out logic_uScriptAct_GetPositionFromTransform_forward_1, out logic_uScriptAct_GetPositionFromTransform_right_1, out logic_uScriptAct_GetPositionFromTransform_up_1, out logic_uScriptAct_GetPositionFromTransform_worldMatrix_1);
         local_2_UnityEngine_Vector3 = logic_uScriptAct_GetPositionFromTransform_position_1;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetPositionFromTransform_uScriptAct_GetPositionFromTransform_1.Out;
         
         if ( test_0 == true )
         {
            Relay_In_7();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CameraScript.uscript at Get Position From Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Finished_3()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("09b60483-802c-4b6b-ae52-0812a897bf66", "Move_To_Location", Relay_Finished_3)) return; 
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CameraScript.uscript at Move To Location.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_3()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("09b60483-802c-4b6b-ae52-0812a897bf66", "Move_To_Location", Relay_In_3)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_MoveToLocation_targetArray_3.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_MoveToLocation_targetArray_3, index + 1);
               }
               logic_uScriptAct_MoveToLocation_targetArray_3[ index++ ] = owner_Connection_4;
               
            }
            {
               logic_uScriptAct_MoveToLocation_location_3 = local_13_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MoveToLocation_uScriptAct_MoveToLocation_3.In(logic_uScriptAct_MoveToLocation_targetArray_3, logic_uScriptAct_MoveToLocation_location_3, logic_uScriptAct_MoveToLocation_asOffset_3, logic_uScriptAct_MoveToLocation_totalTime_3);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CameraScript.uscript at Move To Location.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Cancel_3()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("09b60483-802c-4b6b-ae52-0812a897bf66", "Move_To_Location", Relay_Cancel_3)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_MoveToLocation_targetArray_3.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_MoveToLocation_targetArray_3, index + 1);
               }
               logic_uScriptAct_MoveToLocation_targetArray_3[ index++ ] = owner_Connection_4;
               
            }
            {
               logic_uScriptAct_MoveToLocation_location_3 = local_13_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MoveToLocation_uScriptAct_MoveToLocation_3.Cancel(logic_uScriptAct_MoveToLocation_targetArray_3, logic_uScriptAct_MoveToLocation_location_3, logic_uScriptAct_MoveToLocation_asOffset_3, logic_uScriptAct_MoveToLocation_totalTime_3);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CameraScript.uscript at Move To Location.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_7()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("ce75acdd-f15d-4c43-ab9a-c1ad89cd0762", "Get_Components__Vector3_", Relay_In_7)) return; 
         {
            {
               logic_uScriptAct_GetComponentsVector3_InputVector3_7 = local_2_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_7.In(logic_uScriptAct_GetComponentsVector3_InputVector3_7, out logic_uScriptAct_GetComponentsVector3_X_7, out logic_uScriptAct_GetComponentsVector3_Y_7, out logic_uScriptAct_GetComponentsVector3_Z_7);
         local_6_System_Single = logic_uScriptAct_GetComponentsVector3_X_7;
         local_8_System_Single = logic_uScriptAct_GetComponentsVector3_Z_7;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetComponentsVector3_uScriptAct_GetComponentsVector3_7.Out;
         
         if ( test_0 == true )
         {
            Relay_In_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CameraScript.uscript at Get Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("07a713cd-acb1-4b58-8856-9f6efda4b5fc", "Subtract_Float", Relay_In_9)) return; 
         {
            {
               logic_uScriptAct_SubtractFloat_A_9 = local_8_System_Single;
               
            }
            {
               logic_uScriptAct_SubtractFloat_B_9 = local_10_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_9.In(logic_uScriptAct_SubtractFloat_A_9, logic_uScriptAct_SubtractFloat_B_9, out logic_uScriptAct_SubtractFloat_FloatResult_9, out logic_uScriptAct_SubtractFloat_IntResult_9);
         local_11_System_Single = logic_uScriptAct_SubtractFloat_FloatResult_9;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_SubtractFloat_uScriptAct_SubtractFloat_9.Out;
         
         if ( test_0 == true )
         {
            Relay_In_12();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CameraScript.uscript at Subtract Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_12()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("97d19572-174a-449e-82eb-9f55e7d8a0f9", "Set_Components__Vector3_", Relay_In_12)) return; 
         {
            {
               logic_uScriptAct_SetComponentsVector3_X_12 = local_6_System_Single;
               
            }
            {
            }
            {
               logic_uScriptAct_SetComponentsVector3_Z_12 = local_11_System_Single;
               
            }
            {
            }
         }
         logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_12.In(logic_uScriptAct_SetComponentsVector3_X_12, logic_uScriptAct_SetComponentsVector3_Y_12, logic_uScriptAct_SetComponentsVector3_Z_12, out logic_uScriptAct_SetComponentsVector3_OutputVector3_12);
         local_13_UnityEngine_Vector3 = logic_uScriptAct_SetComponentsVector3_OutputVector3_12;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_SetComponentsVector3_uScriptAct_SetComponentsVector3_12.Out;
         
         if ( test_0 == true )
         {
            Relay_In_3();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CameraScript.uscript at Set Components (Vector3).  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CameraScript.uscript:2", local_2_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "202b4ac0-cb7c-40a0-af67-2b3c603d4f15", local_2_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CameraScript.uscript:5", local_5_UnityEngine_Transform);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "d20700de-0863-4360-8f5d-30fea50f0bda", local_5_UnityEngine_Transform);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CameraScript.uscript:6", local_6_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "632895d6-1165-40e1-a71a-a19b41dde94b", local_6_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CameraScript.uscript:8", local_8_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "94e259d6-1dec-47b3-8cc9-01d3b314c9fd", local_8_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CameraScript.uscript:10", local_10_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "10faae43-808d-461c-8320-48bb91a9a99a", local_10_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CameraScript.uscript:11", local_11_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "faa4321c-56c1-45e9-805e-e2a3a20787b9", local_11_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CameraScript.uscript:13", local_13_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "a30f3940-ec31-4978-9d31-795a26dbf05b", local_13_UnityEngine_Vector3);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
